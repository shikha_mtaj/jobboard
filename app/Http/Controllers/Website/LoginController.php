<?php

namespace App\Http\Controllers\Website;



    use App\Candidate;
    use Illuminate\Support\Facades\Session;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    // Candidate login page
    public function index()
    {

        return view('website.login');

    }

    // Candidate Authentication
    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::guard('candidate')->attempt([
            'email' => $request->email,
            'password' => $request->password,

        ])) {
            //return redirect(route('front.dashboard'));
            return redirect('');
        } else {
            return redirect()->back()->with('error', 'Invalid Username or Password');
        }
    }




    public function logout()
    {
        Auth::guard('candidate')->logout();

        return redirect('');
    }



}
