<?php

namespace App\Http\Controllers\Website;

use App\Jobpost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    // Search Result
    public function index(Request $request){

        /*$this->validate($request ,array(
            'title' => 'required'
        ));*/

        if(!empty($request->title)){ $title = $request->title; } else { $title="ALL"; }
        if(!empty($request->category_id)){ $category_id = $request->category_id; } else { $category_id="ALL"; }

        return redirect(route('website.viewSearchResult',['title' => $title ,'category_id' => $category_id]));

    }

    public function searchResult($title ,$category_id){

        if(!empty($category_id) && !empty($title)){
            if($title == "ALL" && $category_id == "ALL"){
                $searchResult = Jobpost::get();
            } else if($title == "ALL"){
                $searchResult = Jobpost::where('category_id', $category_id )->get();
            } else if($category_id == "ALL"){
                $searchResult = Jobpost::where('job_title', 'like', '%' . $title . '%')->get();
            } else {
                $searchResult = Jobpost::where('category_id', $category_id )->where('job_title', 'like', '%' . $title . '%')->get();
            }
        }

        return view('website.searchresult',['searchResult' => $searchResult]);

    }

}
