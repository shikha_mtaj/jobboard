<?php

namespace App\Http\Middleware\Front;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CandidateDashboardAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Session::has('jobId'));

        if(!Session::has('jobId'))
        {
            return redirect(route('front.job'));
        }
        return $next($request);
    }
}
