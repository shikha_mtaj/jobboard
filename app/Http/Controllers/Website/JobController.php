<?php

namespace App\Http\Controllers\Website;

use App\Candidate;
use App\CandidateJob;
use App\Jobpost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{
    public function index($id)
    {
        if(Auth::guard('candidate')->check())
        {
            $candidateJobs=CandidateJob::where('candidate_id',Auth::guard('candidate')->user()->id)->pluck('jobpost_id')->toarray();;
            $job=Jobpost::find($id);
            return view('website.job_details',['job'=>$job,'candidateJobs'=>$candidateJobs]);
        }
        else{
            $job=Jobpost::find($id);
            return view('website.job_details',['job'=>$job]);
        }
    }

    public function applyJob($id)
    {
        CandidateJob::create([
           'jobpost_id'=>$id,
          'candidate_id' =>Auth::guard('candidate')->user()->id
        ]);

        return redirect()->back()->with('message','You have been Successfully Applied for this Post');

    }





}
