<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendContactEnquiryLink implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $enquiry;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($enquiry)
    {
        //
        $this->enquiry = $enquiry;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $template_path = 'email-template.contactenquiry';

        Mail::send($template_path, ['enquiry'=>$this->enquiry] , function($message)  {
            // Set the receiver and subject of the mail.
            $message->to(env('MAIL_USERNAME'),"Mtaj solutions")->subject('Contact Enquiry : Job Board');
            // Set the sender
            $message->from($this->enquiry->email);
        });
    }
}
