<?php

namespace App\Http\Controllers\admin;

use App\Jobs\SendResetPasswordLink;
use App\PasswordReset;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{
    public function forgotPasswordEmail()
    {

        return view('admin.forgotpassword');
    }

    public function verifyForgotPasswordEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users'
        ]);

        $token = str_random(30);

        PasswordReset::create([
            'email' => $request->email,
            'token' => $token
        ]);

        $this->dispatch(new SendResetPasswordLink($request->email,$token));
        return redirect()->back()->with('info', 'Password reset Link has been sent in your email.');
    }


    public function resetPasswordForm()
    {
   return view('admin.resetpassword');
    }


    public function resetPassword(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|exists:users',
            'password' => 'required',
            'confirm_password' => 'required|same:password'

        ]);

        $user = User::where('email',$request->email)->first();
        $user->password = bcrypt($request['password']);
        $user->save();

        PasswordReset::where('email',$request->email)->delete();

        return redirect(route('admin.dashboard'))->with('message', 'Your Password has been changed successfully.');

    }
}
