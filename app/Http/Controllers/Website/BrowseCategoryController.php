<?php

namespace App\Http\Controllers\Website;

use App\Category;
use App\Jobpost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrowseCategoryController extends Controller
{
    // Browse Categories
    public function index(){

        $categories = Category::orderBy('name','ASC')->where('status',1)->get();
        return view('website.browsecategories', ['categories' => $categories]);

    }

    public function categoryJobs($id){

        $jobs = Jobpost::orderBy('id','DESC')->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->where('status',1)->where('category_id',$id)->get();
        $categories = Category::orderBy('name','ASC')->where('status',1)->get();

        return view('website.browsejobs', ['jobs' => $jobs ,'categories' => $categories]);
    }

}
