@extends('website.pageLayout')

@section('content')

    <!-- Page Header Start -->
    <div class="page-header" style="background: url({{ URL::asset('assets/img/banner1.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Find Job</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('website.home') }}"><i class="ti-home"></i> Home</a></li>
                            <li class="current">Find Job</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <!-- Find Job Section Start -->
    <section class="find-job section">
        <div class="container">
            <h2 class="section-title">Search Jobs</h2>
            <div class="row">
                <div class="col-md-12">
                    @if(!empty($searchResult[0]))
                    @foreach($searchResult AS $job)
                        <div class="job-list">
                            <div class="thumb">
                                <a href="{{ route('website.job_details',['id'=>$job->id]) }}"><img style="width: 100px" src="{{ asset('assets/images/job/'.$job->image)}}" alt=""></a>
                            </div>
                            <div class="job-list-content">
                                <h4><a href="{{ route('website.job_details',['id' => $job->id])}} ">{{ $job->job_title }}</a><span class="full-time">{{ $job->job_type }}</span></h4>
                                <p>{!! $job->short_description !!}</p>
                                <div class="job-tag">
                                    <div class="pull-left">
                                        <div class="meta-tag">
                                            <span><i class="ti-desktop"></i>{{ $job->category->name }}</span>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('website.job_details',['id'=>$job->id]) }}" class="btn btn-common btn-rm">More Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @else
                        <h4>No Jobs Found.</h4>
                    @endif

                </div>
            </div>
        </div>
    </section>
    <!-- Find Job Section End -->
@endsection

@section('js')

@endsection