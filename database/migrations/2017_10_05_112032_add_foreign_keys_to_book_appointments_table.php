<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToBookAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('book_appointments', function(Blueprint $table)
        {
            $table->foreign('appointment_id', 'book_appointments_fk0')->references('id')->on('appointments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('jobpost_id', 'book_appointments_fk1')->references('id')->on('jobposts')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('book_appointments', function(Blueprint $table)
        {
            $table->dropForeign('book_appointments_fk0');
            $table->dropForeign('book_appointments_fk1');
        });
    }
}
