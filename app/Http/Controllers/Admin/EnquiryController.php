<?php

namespace App\Http\Controllers\Admin;

use App\Enquiry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EnquiryController extends Controller
{
    //

    // Enquires listing
    public function index(){

        $enquiries = Enquiry::get();
        return view('admin.enquiry.enquiries',['enquiries' => $enquiries]);

    }

    // delete enquiry
    public function deleteEnquiry(Request $request)
    {
        $this->validate($request, [
            'enquiryId' => 'required|exists:enquiries,id'
        ]);
        $enquiry =Enquiry::find($request->enquiryId);
        $enquiry->delete();
    }

}
