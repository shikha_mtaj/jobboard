<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('book_appointments', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('appointment_id')->index('book_appointments_fk0');
            $table->integer('jobpost_id')->index('book_appointments_fk1');
            $table->text('location', 65535)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('book_appointments');
    }
}
