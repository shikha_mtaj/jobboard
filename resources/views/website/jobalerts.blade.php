@extends('website.pageLayout')

@section('content')

    <!-- Page Header Start -->
    <div class="page-header" style="background: url({{ URL::asset('assets/img/banner1.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Job Alerts</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('website.home') }}"><i class="ti-home"></i> Home</a></li>
                            <li class="current">Job Alerts</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <!-- Start Content -->
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="job-alerts-item">
                        <h3 class="alerts-title">Job Alerts</h3>
                        <div class="alerts-list">
                            <div class="row">
                                <div class="col-md-3">
                                    <p>Name</p>
                                </div>
                                <div class="col-md-3">
                                    <p>Contract Type</p>
                                </div>
                                <div class="col-md-3">
                                    <p>Frequency</p>
                                </div>
                                <div class="col-md-3">
                                    <p>Created At</p>
                                </div>
                            </div>
                        </div>
                        @foreach($latestJobs AS $key => $job)
                        <div class="alerts-content">
                            <div class="row">
                                <div class="col-md-3">
                                    <h3>{{ $job->job_title }}</h3>
                                    <span class="location"><i class="ti-location-pin"></i> {{ $job->category->name }}</span>
                                </div>
                                <div class="col-md-3">
                                    <p><span class="full-time">{{ $job->job_type }}</span></p>
                                </div>
                                <div class="col-md-3">
                                    <p>Daily</p>
                                </div>
                                <div class="col-md-3">
                                    <p>{{ date('m-d-Y',strtotime($job->created_at)) }}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->

@endsection