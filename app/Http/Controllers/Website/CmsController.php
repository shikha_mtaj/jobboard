<?php

namespace App\Http\Controllers\Website;

use App\Cms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CmsController extends Controller
{
    // Cms Data
    public function page($slug){
        $cms = Cms::where(['slug' => $slug])->first();
        return view('website.cms',['cms' => $cms]);
    }
}
