@extends('mainlayout')

@section('title')
    City Partners
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->

            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="{{ route('admin.city') }}">Cities</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>

            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <div class="btn-group">
                                <a href="{{ route('admin.addCityPartner',['id' => $id]) }}" class="btn green">
                                    Add City Partner <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <h3>City Name : {{ $city->name }}</h3>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                            <tr>
                                <th>Sr. No</th>
                                <th>Name</th>
                                <th>Auth Person</th>
                                <th>Contact No</th>
                                <th>Email ID</th>
                                <th>Bonus</th>
                                <th>Join Date</th>
                                <th>Expiry Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($citypartners))
                                @foreach($citypartners AS $key => $partner)
                                    <tr>
                                        <td style="text-align: center;">
                                            {{ $key + 1 }}
                                        </td>
                                        <td>{{ $partner->name }}</td>
                                        <td>{{ $partner->auth_person }}</td>
                                        <td>{{ $partner->contact_no }}</td>
                                        <td>{{ $partner->email }}</td>
                                        <td>{{ $partner->bonus }}</td>
                                        <td>{{ $partner->join_date }}</td>
                                        <td>{{ $partner->expiry_date }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-xs green dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true"> Actions
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="javascript:;" class="deleteCityPartner" id="deleteCityPartner" data-val="{{ $partner->id }}">
                                                            <i class="fa fa-trash-o"></i> Delete </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan="8">No Records Found.</td></tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

@endsection

@section('js')

    <script type="text/javascript">

        $('.deleteCityPartner').on('click', function(e){
            var cityPartnerId = $(this).attr('data-val');
            swal({
                title: 'Are you sure?',
                text: "You want to Delete this City Partner",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.deleteCityPartner') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        cityPartnerId: cityPartnerId
                    },
                    success: function (data) {
                        swal({
                            title: 'Success!',
                            text: 'City Partner has been removed Successfully.',
                            type: "success",
                            confirmButtonText: "OK"
                        }).then(function () {
                            location.reload();
                        });
                    },
                    error: function (xhr, status, error) {
                        swal(
                            'Oops !',
                            'Something went wrong, please try again later.',
                            'warning'
                        );
                    }
                });
            });
        });

    </script>

@endsection