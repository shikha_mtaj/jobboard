@extends('login_mainlayout')


@section('title')
    Reset Password
@endsection

@section('content')

    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-page" action="{{ route('admin.resetPassword') }}" method="post">
        {{ csrf_field() }}
        <h3>Reset Password</h3>

        <div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
            @if($errors->has('email'))
                <span id="name-error" class="help-block help-block-error">{{ $errors->first('email') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('password')? 'has-error':'' }}">
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Password" name="password"/>
            @if($errors->has('password'))
                <span id="name-error" class="help-block help-block-error">{{ $errors->first('password') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('confirm_password')? 'has-error':'' }}">
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Re-type Password" name="confirm_password"/>
            @if($errors->has('confirm_password'))
                <span id="name-error" class="help-block help-block-error">{{ $errors->first('confirm_password') }}</span>
            @endif
        </div>
        <div class="form-actions">
            {{--<button type="button" id="back-btn" onclick="window.location='{{ url('admin') }}'" class="btn btn-default">Back</button>--}}
            <input type="submit" class="btn btn-success uppercase pull-leftS" >

        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->

@endsection