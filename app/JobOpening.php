<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOpening extends Model
{
    //
    protected $fillable = ['jobpost_id','company_name','start_date','end_date','start_time','end_time','location'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jobDetail()
    {
        return $this->belongsTo(Jobpost::class,'jobpost_id','id');
    }
}
