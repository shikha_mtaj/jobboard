<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function profile()
    {

        return view('admin.profile');
    }

    public function updateProfile(Request $request)
    {

        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|email',
            'mobile'=>'required'

        ]);

        $user=Auth::user();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->mobile=$request->mobile;

        $user->save();

        return redirect()->back()->with('message','Your Profile details has been changed successfully.');

    }
}
