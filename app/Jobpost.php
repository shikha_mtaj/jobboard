<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobpost extends Model
{
    //
    protected $fillable = ['job_title','job_description','short_description','image','salary','job_type','start_date','end_date','company_title','education','experiences','skills','responsibilities','place','approved_by','status','category_id'];

     public function category()
  {
      return $this->belongsTo(Category::class, 'category_id', 'id');
  }

}
