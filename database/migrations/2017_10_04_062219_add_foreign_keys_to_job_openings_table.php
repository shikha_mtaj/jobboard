<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToJobOpeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('job_openings', function(Blueprint $table)
        {
            $table->foreign('jobpost_id', 'job_openings_fk0')->references('id')->on('jobposts')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('job_openings', function(Blueprint $table)
        {
            $table->dropForeign('job_openings_fk0');
        });
    }
}
