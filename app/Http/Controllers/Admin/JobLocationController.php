<?php

namespace App\Http\Controllers\Admin;

use App\JobLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class JobLocationController extends Controller
{
    // Job Location Dashboard
    public function index(){

        $joblocations = JobLocation::get();
        return view('admin.joblocation.index',[ 'joblocations' => $joblocations ]);
    }

    // Delete Job Location
    public function deleteJobLocation(Request $request)
    {
        $this->validate($request, [
            'deleteId' => 'required|exists:job_locations,id'
        ]);
        $joblocation =JobLocation::find($request->deleteId);
        $joblocation->delete();
    }

    // Delete All Job Locations
    public function deleteAllJobLocations(Request $request){
        if($request['joblocations']!=""){
            $jobLocations = JobLocation::wherein('id',$request['joblocations'])->get();
            foreach ($jobLocations AS $location){
                $joblocation = JobLocation::find($location->id);
                $joblocation->delete();
            }
        }
    }

    public function uploadJobLocationsExcel(){
        return view('admin.joblocation.uploadjoblocationsexcel');
    }

    // Upload New Job Locations
    public function storeJobLocationsExcel(Request $request){

        if($request->file('file') != ""){
            $image = $request->file('file');
            $excel_file = time().$image->getClientOriginalName();
            $destinationPath = base_path('/assets/excelfiles');
            $image->move($destinationPath, $excel_file);
        } else {
            return redirect(route('admin.uploadJobLocationsExcel'))->with('error','Please upload excel file..');
        }

        $address = "./assets/excelfiles/".$excel_file;

        Excel::load($address, function ($reader) {
            $results = $reader->get();
            foreach ($results->toArray() as $key => $value) {
                if(!empty($value['name'])){
                    JobLocation::create([
                        'name' => $value['name']
                    ]);
                }
            }
        });

        return redirect(route('admin.jobLocation'))->with('message','Excel file data uploaded successfully.');

    }

}
