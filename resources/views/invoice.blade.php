
<h2 style="text-align: center">APPOINTMENT LETTER</h2><br>
<br>

{{ date('d-m-Y') }}<br><br>

Dear {{ $appointment->candidate->name }},<br><br>

Your appointment has been confirmed by admin. <br><br>

Appointment Details : <br/><br/>

<b>City Name :</b> {{ $appointment->city->name }} <br><br>

<b>Categories :</b> {{ $appointment->categories }} <br><br>

<b>Date :</b> {{ $appointment->date }} <br><br>

<b>Start Time :</b> {{ $appointment->start_time }} <br><br>

<b>Total Experiences :</b> {{ $appointment->total_experiences }} <br><br>

<b>Job Location :</b> {{ $appointment->joblocations }} <br><br>

<b>Country :</b> {{ $appointment->country }} <br><br>


Please come with following documents.<br><br>

1. Leaving Certificate<br>
2. SSC Certificate<br>
3. HSC Certificate<br>
3. Adhar Card<br>
4. Election Card<br><br>

<p></p>Sincerely,<br>
Admin,<br>
Job Board</p>
