<?php

namespace App\Http\Controllers\Admin;

use App\Cms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CmsController extends Controller
{
    // Cms dashboard
    public function index(){

        $cms_pages = Cms::get();
        return view('admin.cms.index',[ 'cms_pages' => $cms_pages ]);
    }

    //add cms page
    public function addCms(){
        return view('admin.cms.addcms');
    }

    public function storeCms(Request $request){

        $this->validate($request ,array(
            'title' => 'required',
            'slug' => 'required'
        ));

        $cms = Cms::create([
            'title' => $request->title,
            'slug' => $request->slug,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description,
            'description' => $request->description
        ]);

        $cms->save();
        return redirect(route('admin.cms'))->with('message','CMS details have been saved successfully.');
    }

    public function editCms($id){
        $cms = Cms::find($id);
        return view('admin.cms.editcms',['cms'=>$cms]);
    }

    public function updateCms(Request $request ,$id){

        $this->validate($request ,array(
            'title' => 'required'
        ));

        $cms = Cms::find($id);

        $cms->title = $request->title;
        $cms->slug = $request->slug;
        $cms->meta_keyword = $request->meta_keyword;
        $cms->meta_description = $request->meta_description;
        $cms->description = $request->description;

        $cms->save();

        return redirect(route('admin.cms'))->with('message', 'Cms details has been updated Successfully.');

    }

}
