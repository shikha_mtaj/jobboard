@extends('mainlayout')

@section('title')
    Job Candidates
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->

            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="{{ route('admin.job') }}">Jobs</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>
            <img src="{{ asset('assets/loader.gif') }}" style="display: none;" class="loading-img" width="100px" height="100px">
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                            <tr>
                                <th>Sr. No</th>
                                <th>Candidate Name</th>
                                <th>Email ID</th>
                                <th>Mobile No</th>
                                <th>Applied Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($jobCandidates AS $key => $candidate)
                                <tr>
                                    <td style="text-align: center;">
                                       {{ $key + 1 }}
                                    </td>
                                    <td>{{ $candidate->candidateDetail->name }}</td>
                                    <td>{{ $candidate->candidateDetail->email }}</td>
                                    <td>{{ $candidate->candidateDetail->mobile }}</td>
                                    <td>{{ $candidate->candidateDetail->created_at }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-xs green dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" class="deleteJobCandidate" id="deleteJobCandidate" data-val="{{ $candidate->id }}">
                                                        <i class="fa fa-trash-o"></i> Delete </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

@endsection

@section('js')

    <script type="text/javascript">

        $('.deleteJobCandidate').on('click', function(e){
            var jobCandidateId = $(this).attr('data-val');
            swal({
                title: 'Are you sure?',
                text: "You want to Delete this Job Candidate",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.deleteJobCandidate') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        jobCandidateId: jobCandidateId
                    },
                    success: function (data) {
                        swal({
                            title: 'Success!',
                            text: 'Job Candidate has been removed Successfully.',
                            type: "success",
                            confirmButtonText: "OK"
                        }).then(function () {
                            location.reload();
                        });
                    },
                    error: function (xhr, status, error) {
                        swal(
                            'Oops !',
                            'Something went wrong, please try again later.',
                            'warning'
                        );
                    }
                });
            });
        });

        $("#send_mail").click(function(){
            var jobCandidates = [];
            $.each($("input[name='jobcandidates[]']:checked"), function(){
                jobCandidates.push($(this).val());
            });

            if(jobCandidates != ""){

            swal({
                title: 'Are you sure?',
                text: "You want to approved this candidates",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.approveJobCandidate') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        jobCandidates: jobCandidates
                    },
                    beforeSend:function(){
                        $('.loading-img').css('display','');
                    },
                    success: function (data) {
                        swal({
                            title: 'Success!',
                            text: 'Mail sent successfully to candidates',
                            type: "success",
                            confirmButtonText: "OK"
                        }).then(function () {
                            location.reload();
                        });
                    },
                    error: function (xhr, status, error) {
                        $('.loading-img').css('display','none');
                        swal(
                            'Oops !',
                            'Something went wrong, please try again later.',
                            'warning'
                        );
                    }
                });
            });

            } else {

                swal(
                    'Oops !',
                    'Please select job candidates !!',
                    'warning'
                );

            }
        });



    </script>

@endsection