@extends('mainlayout')

@section('title')
    Categories
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->

            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="{{ route('admin.category') }}">Categories</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <div class="btn-group">
                                <a href="{{ route('admin.addCategory')  }}" class="btn green">
                                    Add Category <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                            <tr>
                                <th>Category Code</th>
                                <th>Category Name</th>
                                <th>Status</th>
                                <th width="145px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories AS $category)
                                <tr>
                                    <td>{{ $category->code }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td style="text-align: center;"> <button type="button" data-val="{{ $category->id }}"
                                                                             id="status_{{ $category->id }}"
                                                                             class="status_check {{ ($category['status']) ? 'uk-badge uk-badge-success' : 'uk-badge uk-badge-danger'}}"
                                                                             value="{{$category->status}}">{{ ($category['status']) ? 'Active' : 'Inactive'}}
                                        </button></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-xs green dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a  href="{{ route('admin.editCategory',['id' => $category->id]) }}">
                                                        <i class="fa fa-pencil"></i> Edit </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" class="deleteCategory" id="deleteCategory" data-val="{{ $category->id }}">
                                                        <i class="fa fa-trash-o"></i> Delete </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection

@section('js')


    <script type="text/javascript">

        $('.status_check').click(function () {
            var status = parseInt($(this).val());
            var categoryId = $(this).attr('data-val');

            swal({
                title: 'Are you sure?',
                text: "You want to " + (status ? 'De-Activate' : 'Activate') + " this Category!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.changeStatusCategory') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        status: status,
                        categoryId: categoryId
                    },
                    beforeSend:function(){
                        $('.loading-img').css('display','');
                    },
                    success: function (data) {
                        var element = $('#status_' + categoryId);
                        if (data['status']) {
                            element.html('Active')
                                .removeClass('uk-badge uk-badge-danger')
                                .addClass('uk-badge uk-badge-success');
                        } else {
                            element.html('Inactive')
                                .removeClass('uk-badge uk-badge-success')
                                .addClass('uk-badge uk-badge-danger');
                        }
                        element.val(data['status']);
                        $('.loading-img').css('display','none');
                        swal(
                            (status ? 'De-Activated' : 'Activated') + '!',
                            'Category has been ' + (status ? 'De-Activated' : 'Activated') + ' .',
                            "success"
                        );
                    },
                    error: function (xhr, status, error) {
                        swal(
                            'Oops !',
                            'Something went wrong, please try again later.',
                            'warning'
                        );
                    }
                });
            });
        });

        $('.deleteCategory').on('click', function(e){

            var categoryId = $(this).attr('data-val');

            swal({
                title: 'Are you sure?',
                text: "You want to Delete this Category",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.deleteCategory') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        categoryId: categoryId
                    },
                    success: function (data) {
                        swal({
                            title: 'Success!',
                            text: 'Category has been removed Successfully.',
                            type: "success",
                            confirmButtonText: "OK"
                        }).then(function () {
                            location.reload();
                        });
                    },
                    error: function (xhr, status, error) {
                        swal(
                            'Oops !',
                            'Something went wrong, please try again later.',
                            'warning'
                        );
                    }
                });
            });
        });

    </script>


@endsection