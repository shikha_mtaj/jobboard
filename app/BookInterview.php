<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookInterview extends Model
{
    //
    protected $fillable = ['code','category_id','jobpost_id','resume','date','start_time','location','country'];
}
