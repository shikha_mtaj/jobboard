@extends('website.pageLayout')
@section('content')
    <!-- Page Header Start -->
    <div class="page-header" style="background: url({{ URL::asset('assets/img/banner1.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">My Account</h2>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="ti-home"></i> Home</a></li>
                            <li class="current">login</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <div id="content" class="my-account">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-6 cd-user-modal">
                    <div class="my-account-form">
                        <ul class="cd-switcher">
                            <li style="width: 100%;"><a  href="#0">LOGIN</a></li>

                        </ul>
                        <!-- Login -->

                        <form method="post" action="{{ route('website.authenticate') }}">
                            {{ csrf_field() }}
                            <div id="cd-login" class="is-selected">
                                <div class="page-login-form">
                                    <form role="form" class="login-form">
                                        <div class="form-group {{$errors->has('email')? 'has-error':''}}">
                                            <div class="input-icon ">
                                                <i class="ti-user"></i>
                                                <input type="text" id="email" class="form-control" name="email" placeholder="Username">
                                            </div>
                                        </div>
                                        <div class="form-group {{$errors->has('password')? 'has-error':''}}">
                                            <div class="input-icon">
                                                <i class="ti-lock"></i>
                                                <input type="password" class="form-control" name="password" placeholder="Password">
                                            </div>
                                        </div>
                                        <input type="submit" class="btn btn-common log-btn" value="Login">
                                        {{--<div class="checkbox-item">--}}

                                            {{--<p class="cd-form-bottom-message"><a href="#0">Lost your password?</a></p>--}}
                                        {{--</div>--}}
                                    </form>
                                </div>
                            </div>
                        </form>



                        <div class="page-login-form" id="cd-reset-password"> <!-- reset password form -->
                            <p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>
                            <form class="cd-form">
                                <div class="form-group">
                                    <div class="input-icon">
                                        <i class="ti-email"></i>
                                        <input type="text" id="sender-email" class="form-control" name="email" placeholder="Email">
                                    </div>
                                </div>
                                <p class="fieldset">
                                    <button class="btn btn-common log-btn" type="submit">Reset password</button>
                                </p>
                            </form>
                            <p class="cd-form-bottom-message"><a href="#0">Back to log-in</a></p>
                        </div> <!-- cd-reset-password -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection