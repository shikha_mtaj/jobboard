@extends('mainlayout')

@section('title')
    Add Job
@endsection

@section('content')
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="{{ route('admin.job') }}">Job</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Add</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Add New Job
                                        </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse">
                                            </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config">
                                            </a>
                                            <a href="javascript:;" class="reload">
                                            </a>
                                            <a href="javascript:;" class="remove">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="horizontal-form" method="post" action="{{ route('admin.storeJob') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('job_title') ? 'has-error' : '' }}">
                                                            <label class="control-label">Title</label>
                                                            <input type="text" name="job_title" id="job_title" class="form-control"  value="{{ old('job_title') }}">
                                                            @if ($errors->has('job_title'))
                                                            <span class="help-block">{{ $errors->first('job_title') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('job_type') ? 'has-error' : '' }}">
                                                            <label class="control-label">Job Type</label>
                                                            <input type="text" name="job_type" id="job_type" class="form-control"  value="{{ old('job_type') }}">
                                                            @if ($errors->has('job_type'))
                                                                <span class="help-block">{{ $errors->first('job_type') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <div class="form-group">
                                                            <label>Short Description</label>
                                                            <textarea class="ckeditor form-control" name="short_description" rows="6">{{ old('short_description') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <div class="form-group">
                                                            <label>Description</label>
                                                            <textarea class="ckeditor form-control" name="job_description" rows="6">{{ old('job_description') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('salary') ? 'has-error' : '' }}">
                                                            <label>Salary</label>
                                                            <input type="text" name="salary" id="salary" class="form-control" value="{{ old('salary') }}">
                                                            @if ($errors->has('salary'))
                                                                <span class="help-block">{{ $errors->first('salary') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Category</label>
                                                            <select name="category" id="category" class="form-control">
                                                                <option value="" selected disabled>Select Category </option>
                                                                @foreach($categories as $category )
                                                                <option value="{{ $category->id}}">{{ $category->name }}</option>
                                                              @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                                                            <label class="control-label">Start Date</label>
                                                            <input type="text" data-date-start-date="-{{ date('d') - 1 }}d" data-date="2012-02-12" data-date-format="yyyy-mm-dd" name="start_date" id="start_date" class="form-control date-picker" placeholder="yyyy-mm-dd" value="{{ old('start_date') }}" readonly>
                                                            @if ($errors->has('start_date'))
                                                                <span class="help-block">{{ $errors->first('start_date') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                                                            <label class="control-label">End Date</label>
                                                            <input type="text" data-date-start-date="-{{ date('d') - 1 }}d" data-date="2012-02-12" data-date-format="yyyy-mm-dd" name="end_date" id="end_date" class="form-control date-picker" placeholder="yyyy-mm-dd" value="{{ old('end_date') }}" readonly>
                                                            @if ($errors->has('end_date'))
                                                                <span class="help-block">{{ $errors->first('end_date') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('company_title') ? 'has-error' : '' }}">
                                                            <label class="control-label">Company Title</label>
                                                            <input type="text" name="company_title" id="company_title" class="form-control"  value="{{ old('company_title') }}">
                                                            @if ($errors->has('company_title'))
                                                                <span class="help-block">{{ $errors->first('company_title') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('education') ? 'has-error' : '' }}">
                                                            <label class="control-label">Education</label>
                                                            <input type="text" name="education" id="education" class="form-control"  value="{{ old('education') }}">
                                                            @if ($errors->has('education'))
                                                                <span class="help-block">{{ $errors->first('education') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('skills') ? 'has-error' : '' }}">
                                                            <label class="control-label">Skills</label>
                                                            <input type="text" name="skills" id="skills" class="form-control"  value="{{ old('skills') }}">
                                                            @if ($errors->has('skills'))
                                                                <span class="help-block">{{ $errors->first('skills') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('responsibilities') ? 'has-error' : '' }}">
                                                            <label class="control-label">Responsibilities</label>
                                                            <input type="text" name="responsibilities" id="responsibilities" class="form-control"  value="{{ old('responsibilities') }}">
                                                            @if ($errors->has('responsibilities'))
                                                                <span class="help-block">{{ $errors->first('responsibilities') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('experiences') ? 'has-error' : '' }}">
                                                            <label class="control-label">Experiences</label>
                                                            <input type="text" name="experiences" id="experiences" class="form-control"  value="{{ old('experiences') }}">
                                                            @if ($errors->has('experiences'))
                                                                <span class="help-block">{{ $errors->first('experiences') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('place') ? 'has-error' : '' }}">
                                                            <label class="control-label">Place</label>
                                                            <input type="text" name="place" id="place" class="form-control"  value="{{ old('place') }}">
                                                            @if ($errors->has('place'))
                                                                <span class="help-block">{{ $errors->first('place') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Image</label>
                                                            <input class="btn green" type="file" name="image" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--/span-->
                                            </div>
                                            <div class="form-actions right">
                                                <a href="{{ route('admin.job') }}"><button type="button" class="btn default">Cancel</button></a>
                                                <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection

