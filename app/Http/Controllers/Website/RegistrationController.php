<?php

namespace App\Http\Controllers\Website;

use App\Candidate;
use App\CandidateOtps;
use App\Category;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    // Candidate login page
    public function index()
    {
        $categories = Category::get();
        return view('website.register',['categories' => $categories]);
    }

    public function storeCandidate(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'mobile' => 'required|size:10',
            'gender' => 'required',
            'nationality' =>'required',
            'photo' => 'mimes:doc,docx,pdf|max:1024'
        ));

        if(Candidate::where(['email'=>$request->email])->exists())
        {
            return redirect()->back()->with('error', 'This email has already been Registered.');
        } else {

            $otp = random_int(1111, 9999);
            CandidateOtps::create([
                'mobile' => $request->mobile,
                'otp' => $otp,
            ]);

           /* $response = (new \GuzzleHttp\Client)->request('POST', 'http://www.smsidea.co.in/sendsms.aspx', [
                'form_params' => [
                    'mobile' => '9512018399',
                    'pass' => 'jigar#1234',
                    'senderid' => 'SMSWEB',
                    'to' => $request->mobile,
                    'msg' => 'Dear ' . $request->name . ', Your OTP is ' . $otp,
                ]
            ]); */

            $path = base_path('/assets/front/candidate_documents');

            if ($request->file('photo') != "") {
                $photo = $request->file('photo');
                $input_photo = 'resume' . time() . '.' . $photo->getClientOriginalExtension();
                $destinationPath = $path;
                $photo->move($destinationPath, $input_photo);
            } else {
                $input_photo = '';
            }

            if(!empty($request->category_ids)){
                $category_ids = implode(',',$request->category_ids);
            } else {
                $category_ids = "";
            }

            Session::put('name', $request->name);
            Session::put('email', $request->email);
            Session::put('password', $request->password);
            Session::put('photo', $input_photo);
            Session::put('mobile', $request->mobile);
            Session::put('gender', $request->gender);
            Session::put('category_ids',$category_ids);
            Session::put('nationality', $request->nationality);

            return redirect(route('website.otpForm'))->with('message', 'OTP has been send successfully on your Mobile.');

        }
    }

    public function otpForm()
    {
        return view('website.otp');
    }

    public function varifyOtp(Request $request)
    {
        $this->validate($request, array(
            'otp' => 'required',
        ));

        if (CandidateOtps::where('mobile', Session::get('mobile'))->where('otp', $request->otp)->exists()) {
            CandidateOtps::where('mobile', Session::get('mobile'))->where('otp', $request->otp)->delete();

            $candidate = Candidate::create([
                'name' => Session::get('name'),
                'email' => Session::get('email'),
                'password' => bcrypt(Session::get('password')),
                'mobile' => Session::get('mobile'),
                'category_ids' => Session::get('category_ids'),
                'gender' => Session::get('gender'),
                'nationality' => Session::get('nationality'),
                'resume' => Session::get('photo')
            ]);

            Session::flush();

            return redirect('')->with('message', 'Registration has been done Successfully.');
        } else {
            return redirect()->back()->with('error', 'Invalid OTP.');
        }
    }


    public function logout()
    {
        Auth::guard('candidate')->logout();
        Session::flush();
        return redirect('');
    }


}
