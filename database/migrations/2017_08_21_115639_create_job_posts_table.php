<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobPostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobposts', function(Blueprint $table)
		{
			$table->integer('id', true);
            $table->integer('category_id');
			$table->string('job_title');
            $table->text('short_description', 65535)->nullable();
			$table->text('job_description', 65535)->nullable();
			$table->string('image')->nullable();
			$table->string('salary')->nullable();
			$table->string('job_type')->nullable();
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('company_title')->nullable();
            $table->string('education')->nullable();
            $table->string('experiences')->nullable();
            $table->string('skills')->nullable();
            $table->string('responsibilities')->nullable();
            $table->string('place')->nullable();
            $table->integer('approved_by')->comment('1:Admin, 2:Staff')->nullable();
            $table->integer('status')->comment('1:Active, 0:Inactive')->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobposts');
	}

}
