<?php

namespace App\Http\Controllers\Website;

use App\Enquiry;
use App\Jobs\SendEnquiryLink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EnquiryController extends Controller
{
    //
    public function index(){

        return view('website.enquiry');
    }

    public function storeEnquiry(Request $request){

        $this->validate($request, array(
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required'
        ));

        $enquiry = Enquiry::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message'=>$request->message
        ]);

        $enquiry->save();
        $this->dispatch(new SendEnquiryLink($enquiry));
        return redirect('')->with('message', 'Enquiry details has been sent successfully.');

    }



}
