<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function changePasswordForm()
    {

        return view('admin.changepassword');
    }

    public function updatePassword(Request $request)
    {

        $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'

        ]);

        if (!\Hash::check($request->get('old_password'), Auth::user()->password)) {
            return redirect()->back()->with('error', 'You have entered incorrect Old Password');


        }

        $user = Auth::user();
        $user->password = bcrypt($request['new_password']);

        $user->save();

        return redirect(route('admin.dashboard'))->with('message', 'Your Password has been changed successfully.');

    }
}
