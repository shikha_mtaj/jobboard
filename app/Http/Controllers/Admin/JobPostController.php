<?php

namespace App\Http\Controllers\Admin;


use App\Candidate;
use App\CandidateJob;
use App\CandidateResult;
use App\Category;
use App\Job;
use App\JobOpening;
use App\Jobpost;
use App\JobQuestion;
use App\Jobs\SendCandidateJobApprovalLink;
use App\QuestionAnswer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class JobPostController extends Controller
{

    public function index(){

        $jobs = Jobpost::with('category')->get();

        return view('admin.jobpost.index',[ 'jobs' => $jobs]);
    }

    public function addJob(){
        $categories=Category::where('status',1)->get();
        return view('admin.jobpost.addjob',['categories'=>$categories ]);
    }

    public function storeJob(Request $request){

//        dd($request);
        $this->validate($request ,array(
            'job_title' => 'required',
            //'image' => 'required',
            'job_type' => 'required',
            'short_description' => 'required',
            'job_description' => 'required',
            'salary' => 'required',
            'category' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ));

        if($request->file('image') != ""){
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = base_path('/assets/images/job');
            $image->move($destinationPath, $input['imagename']);
        } else {
            $input['imagename'] = "";
        }

        $job = Jobpost::create([
            'job_title' => $request->job_title,
            'job_description' => $request->job_description,
            'short_description' => $request->short_description,
            'image' => $input['imagename'],
            'salary' => $request->salary,
            'job_type' => $request->job_type,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'company_title' => $request->company_title,
            'education' => $request->education,
            'experiences' => $request->experiences,
            'skills' => $request->skills,
            'responsibilities' => $request->responsibilities,
            'place' => $request->place,
            'status' => 1,
            'approved_by' => 1,
            'category_id' => $request->category
        ]);

    return redirect(route('admin.job'))->with('message','Job details has been saved successfully.');
    }

    public function deleteJob(Request $request)
    {

        $this->validate($request, [
            'jobId' => 'required|exists:jobposts,id'
        ]);

        $job = Jobpost::find($request->jobId);
        $job->delete();

    }

    public function editJob($id){

        $job = Jobpost::with('category')->find($id);
        $categories=Category::where('status',1)->get();
        return view('admin.jobpost.editjob',['job'=>$job,'categories'=>$categories]);
    }

    public function updateJob(Request $request ,$id){


        $this->validate($request ,array(
            'job_title' => 'required',
            //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'job_type' => 'required',
            'short_description' => 'required',
            'job_description' => 'required',
            'salary' => 'required',
            'category'=>'required'
        ));

        if($request->file('image')!=""){
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = base_path('/assets/images/job');
            $image->move($destinationPath, $input['imagename']);
        } else {
            $input['imagename'] = $request['hiddenImage'];
        }

        $job = Jobpost::find($id);

        $job->job_title = $request->job_title;
        $job->job_description = $request->job_description;
        $job->short_description = $request->short_description;
        $job->image = $input['imagename'];
        $job->salary = $request->salary;
        $job->job_type = $request->job_type;
        $job->start_date = $request->start_date;
        $job->end_date = $request->end_date;
        $job->company_title = $request->company_title;
        $job->education = $request->education;
        $job->experiences = $request->experiences;
        $job->skills = $request->skills;
        $job->responsibilities = $request->responsibilities;
        $job->place = $request->place;
        $job->category_id=$request->category;

        $job->save();

        return redirect(route('admin.job'))->with('message', 'Job details has been updated Successfully.');

    }

    public function viewJobCandidates($id){

        $jobCandidates = CandidateJob::where(['jobpost_id' => $id])->get();
        return view('admin.jobpost.viewjobcandidates',['jobCandidates' => $jobCandidates]);

    }

    // Approve job candidates
    public function approveJobCandidate(Request $request)
    {
        if($request['jobCandidates'] != ""){

            $candidatejobs = CandidateJob::wherein('id',$request['jobCandidates'])->get();
            foreach ($candidatejobs AS $job){

                $candidateDetails = Candidate::find($job->candidate_id);
                $jobDetails = Jobpost::find($job->jobpost_id);
                //$candidates[]=$candidateDetails->phone;

                $this->dispatch(new SendCandidateJobApprovalLink($candidateDetails ,$jobDetails));
                CandidateJob::where(['id' => $job->id])->update(['flag' => 1,'payment_status' => 1]);

            }
        }
    }

    // delete candidate
    public function deleteJobCandidate(Request $request)
    {
        $this->validate($request, [
            'jobCandidateId' => 'required|exists:candidate_jobs,id'
        ]);

        $jobcandidate =CandidateJob::find($request->jobCandidateId);
        $jobcandidate->delete();
    }

    // View Job Candidate Result
    public function viewJobCandidateResult($id){

        $candidateJob = CandidateJob::find($id);
        $candidateJobResult = CandidateResult::where(['candidate_id' => $candidateJob->candidate_id ,'jobpost_id' => $candidateJob->jobpost_id])->first();

        return view('admin.jobpost.viewjobresult',['candidateJobResult' => $candidateJobResult]);
    }

    // job status change
    public function changeStatus(Request $request)
    {
        $this->validate($request, [
            'jobId' => 'required|exists:jobposts,id'
        ]);

        $job = Jobpost::find($request['jobId']);
        if ($request['status']) { $job->status = '0'; }
        else { $job->status = '1'; }

        $job->save();

        return response()->json([
            'status' => $request['status'] ? 0 : 1
        ]);
    }

    // view job opening
    public function viewJobOpenings($id){

        $jobopenings = JobOpening::where(['jobpost_id'=>$id])->get();
        $job = Jobpost::find($id);
        return view('admin.jobpost.viewjobopenings',['jobopenings' => $jobopenings ,'id' => $id ,'job' => $job]);

    }

    // add job opening
    public function addJobOpening($id){
        return view('admin.jobpost.addjobopening',['id' => $id]);
    }

    // store job opening
    public function storeJobOpening(Request $request){

        $this->validate($request ,array(
            'company_name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'location' => 'required'
        ));

        JobOpening::create([
            'jobpost_id' => $request->jobpost_id,
            'company_name' => $request->company_name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'location' => $request->location
        ]);

        return redirect(route('admin.viewJobOpenings',['id' => $request->jobpost_id]))->with('message','Job Opening details has been saved successfully.');
    }

    // delete job opening
    public function deleteJobOpening(Request $request){

        $this->validate($request, [
            'jobOpeningId' => 'required|exists:job_openings,id'
        ]);

        $jobopening = JobOpening::find($request->jobOpeningId);
        $jobopening->delete();

    }

    // upload jobs using excel
    public function uploadJobsExcel(){




    }


}
