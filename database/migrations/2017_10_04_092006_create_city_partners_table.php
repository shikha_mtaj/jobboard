<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('city_partners', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('city_id')->index('city_partners_fk0');
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->string('auth_person')->nullable();
            $table->string('address')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('email')->nullable();
            $table->string('bonus')->nullable();
            $table->string('join_date')->nullable();
            $table->string('expiry_date')->nullable();
            $table->text('description', 65535)->nullable();
            $table->string('reference')->nullable();
            $table->integer('approved_by')->comment('1:Admin, 2:Staff')->nullable();
            $table->integer('status')->comment('1:Active, 0:Inactive')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('city_partners');
    }
}
