@extends('login_mainlayout')

@section('title')
    Login
@endsection

@section('content')
<!-- BEGIN LOGIN FORM -->
<form class="login-form" action="{{ route('admin.authenticate') }}" method="post">
    {{ csrf_field() }}
    <h3 class="form-title">Sign In</h3>
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <span>
			Enter any username and password. </span>
    </div>
    <div class="form-group {{$errors->has('email')? 'has-error':''}}">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
        @if ($errors->has('email'))
                <span id="name-error" class="help-block help-block-error">{{ $errors->first('email') }}</span>
        @endif
    </div>
    <div class="form-group {{$errors->has('password')?'has-error':'' }}">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
   @if($errors->has('password'))
            <span id="name-error" class="help-block help-block-error">{{ $errors->first('password') }}</span>
       @endif
    </div>
    <div class="form-actions">
        <button type="submit" class="btn btn-success uppercase">Login</button>

        {{--<a href="{{ route('admin.forgotPassword') }}" id="forget-password" class="forget-password">Forgot Password?</a>--}}
    </div>
</form>
<!-- END LOGIN FORM -->

  @endsection