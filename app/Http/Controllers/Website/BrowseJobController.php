<?php

namespace App\Http\Controllers\Website;

use App\Category;
use App\Jobpost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrowseJobController extends Controller
{
    // Browse Jobs
    public function index(){

        $jobs = Jobpost::orderBy('id','DESC')->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->where('status',1)->get();
        $categories = Category::orderBy('name','ASC')->where('status',1)->get();

        return view('website.browsejobs', ['jobs' => $jobs ,'categories' => $categories]);

    }

}
