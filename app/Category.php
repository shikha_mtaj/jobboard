<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['code','name'];

    public function categoryJobs(){

        return $this->hasMany(Jobpost::class,'category_id','id');

    }

}
