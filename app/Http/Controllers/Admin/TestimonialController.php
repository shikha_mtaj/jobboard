<?php

namespace App\Http\Controllers\Admin;

use App\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestimonialController extends Controller
{
    // Testimonial dashboard
    public function index(){
        $testimonials = Testimonial::get();
        return view('admin.testimonial.index',[ 'testimonials' => $testimonials ]);
    }

    //add testimonial
    public function addTestimonial(){
        return view('admin.testimonial.addtestimonial');
    }

    public function storeTestimonial(Request $request){

        $this->validate($request ,array(
            'name' => 'required'
        ));

        if($request->file('image') != ""){
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = base_path('/assets/front/testimonials');
            $image->move($destinationPath, $input['imagename']);
        } else {
            $input['imagename'] = "";
        }

        if(isset($request->designation) && !empty($request->designation)){
            $designation = $request->designation;
        } else {
            $designation = "";
        }

        if(isset($request->description) && !empty($request->description)){
            $description = $request->description;
        } else {
            $description = "";
        }

        $testimonial = Testimonial::create([
            'name' => $request->name,
            'designation' => $request->designation,
            'image' => $input['imagename'],
            'description' => $description
        ]);

        $testimonial->save();
        return redirect(route('admin.testimonial'))->with('message','Testimonial details have been saved successfully.');
    }

    public function editTestimonial($id){
        $testimonial = Testimonial::find($id);
        return view('admin.testimonial.edittestimonial',['testimonial'=>$testimonial]);
    }

    public function updateTestimonial(Request $request ,$id){

        $this->validate($request ,array(
            'name' => 'required'
        ));

        if($request->file('image')!=""){
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = base_path('/assets/front/testimonials');
            $image->move($destinationPath, $input['imagename']);
        } else {
            $input['imagename'] = $request['hiddenImage'];
        }

        if(isset($request->designation) && !empty($request->designation)){
            $designation = $request->designation;
        } else {
            $designation = "";
        }

        if(isset($request->description) && !empty($request->description)){
            $description = $request->description;
        } else {
            $description = "";
        }

        $testimonial = Testimonial::find($id);

        $testimonial->name = $request->name;
        $testimonial->designation = $designation;
        $testimonial->image = $input['imagename'];
        $testimonial->description = $description;

        $testimonial->save();

        return redirect(route('admin.testimonial'))->with('message', 'Testimonial details has been updated Successfully.');
    }

    // delete category
    public function deleteTestimonial(Request $request)
    {
        $this->validate($request, [
            'testimonialId' => 'required|exists:testimonials,id'
        ]);
        $testimonial = Testimonial::find($request->testimonialId);
        $testimonial->delete();
    }
}
