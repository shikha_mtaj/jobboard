<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToNewAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('appointments', function(Blueprint $table)
        {
            $table->foreign('candidate_id', 'appointments_fk2')->references('id')->on('candidates')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('city_id', 'appointments_fk3')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('appointments', function(Blueprint $table)
        {
            $table->dropForeign('appointments_fk2');
            $table->dropForeign('appointments_fk3');
        });
    }
}
