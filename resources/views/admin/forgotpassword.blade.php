@extends('login_mainlayout')


@section('title')
    Forgot Password
@endsection

@section('content')

<!-- BEGIN FORGOT PASSWORD FORM -->
<form class="forget-page" action="{{ route('admin.verifyEmail') }}" method="post">
    {{ csrf_field() }}
    <h3>Forget Password ?</h3>
    <p>
        Enter your e-mail address below to reset your password.
    </p>
    <div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
        @if($errors->has('email'))
            <span id="name-error" class="help-block help-block-error">{{ $errors->first('email') }}</span>
        @endif
    </div>
    <div class="form-actions">
          <button type="button" id="back-btn" onclick="window.location='{{ url('admin') }}'" class="btn btn-default">Back</button>
        <input type="submit" class="btn btn-success uppercase pull-right" >

    </div>
</form>
<!-- END FORGOT PASSWORD FORM -->

  @endsection