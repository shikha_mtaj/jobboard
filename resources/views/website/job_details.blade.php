@extends('website.pageLayout')
@section('content')
    <!-- Page Header Start -->
    <div class="page-header" style="background: url({{ asset('/assets/img/banner1.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Job Details</h2>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="ti-home"></i> Home</a></li>
                            <li class="current">Job Details</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <!-- Job Detail Section Start -->
    <section class="job-detail section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-8">
                    <div class="content-area">
                        <h2 class="medium-title">Job Information</h2>
                        <div class="box">
                            <div class="text-left">
                                <h3><a href="#">{{ $job->job_title }}</a></h3>
                                {{--<p>{{ $job->category->name }} </p>--}}
                                <div class="meta">
                                    <span><a href="#"><i class="ti-desktop"></i> {{ $job->category->name }} </a></span>
                                    {{--<span><a href="#"><i class="ti-calendar"></i> Dec 30, 2017 - Feb 20, 2018</a></span>--}}
                                </div>
                                <strong class="price"><i class="fa fa-money"></i>{{ $job->salary }}</strong>
                                {{--<a href="#" class="btn btn-border btn-sm">Freelance</a>--}}

                                @if(Auth::guard('candidate')->check())
                                    @if(in_array($job->id,$candidateJobs))
                                    <a href="#" class="btn btn-common btn-sm">Applied</a>
                                @else
                                        <a href="{{ route('website.applyJob',['id'=>$job->id]) }}" class="btn btn-common btn-sm">Apply For This Job</a>
                                @endif
                                @else
                                    <a href="{{ route('website.login') }}" class="btn btn-common btn-sm">Apply For This Job</a>
                                @endif
                            </div>
                            <div class="clearfix">
                                <p>{!! $job->job_description !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Job Detail Section End -->
@endsection