<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="Jobboard">

    <title>Amigos Management F.Z.C</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/img/amigos - divyang icon.png') }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/jasny-bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css')}}" type="text/css">
    <!-- Material CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/material-kit.css')}}" type="text/css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{ asset('assets/fonts/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/fonts/themify-icons.css')}}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('assets/extras/animate.css')}}" type="text/css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('assets/extras/owl.carousel.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/extras/owl.theme.css')}}" type="text/css">
    <!-- Rev Slider CSS -->
    <link rel="stylesheet" href="{{ asset('assets/extras/settings.css')}}" type="text/css">
    <!-- Slicknav js -->
    <link rel="stylesheet" href="{{ asset('assets/css/slicknav.css')}}" type="text/css">
    <!-- Main Styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/main.css')}}" type="text/css">
    <!-- Responsive CSS Styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css')}}" type="text/css">

    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/colors/red.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/sweetalert/sweetalert2.css') }}"/>

</head>
<body>

<!-- Header Section Start -->
<div class="header">
    <!-- Start intro section -->
    <section id="intro" class="section-intro">
        <div class="logo-menu">
            <nav class="navbar navbar-default" role="navigation" data-spy="affix" data-offset-top="50">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand logo" href="{{ route('website.home') }}"><img width="50px" height="50px" src="assets/img/amigos - divyang icon.png" alt=""></a>
                    </div>

                    <div class="collapse navbar-collapse" id="navbar">
                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav">
                            <li>
                                <a @if (Request::is('/')) class="active" @endif href="{{ route('website.home') }}">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a @if (Request::is('about') || Request::is('cms/job-detail')) class="active" @endif href="{{ route('website.about') }}">
                                    About <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown">
                                    <li><a @if (Request::is('about')) class="active" @endif href="{{ route('website.about') }}">About</a></li>
                                    <li><a @if (Request::is('cms/job-detail')) class="active" @endif href="{{ route('website.cms',['id' => 'job-detail']) }}">Job Detail</a></li>
                                </ul>
                            </li>
                            <li><a @if (Request::is('browse/jobs') || Request::is('browse/categories') || Request::is('job/alerts') || Request::is('take/appointment')) class="active" @endif href="#">Candidates <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown">
                                    <li><a href="{{ route('website.browseJobs') }}">Browse Jobs</a></li>
                                    <li><a href="{{ route('website.browseCategories') }}">Browse Categories</a></li>
                                    @if(Auth::guard('candidate')->check())
                                    <li><a @if(Request::is('take/appointment')) class="active" @endif href="{{ route('website.takeAppointment') }}">Appointment</a></li>
                                        <li><a @if(Request::is('book/interview')) class="active" @endif href="{{ route('website.bookInterview') }}">Book Interview</a></li>
                                    @endif
                                    <li><a href="{{ route('website.jobAlerts') }}">Job Alerts</a></li>
                                </ul>
                            </li>
                            <li>
                                <a @if (Request::is('contact')) class="active" @endif href="{{ route('website.contact') }}">
                                    Contact
                                </a>
                            </li>
                            <li>
                                <a @if (Request::is('enquiry')) class="active" @endif href="{{ route('website.enquiry') }}">
                                    Enquiry
                                </a>
                            </li>

                        </ul>
                        <ul class="nav navbar-nav navbar-right float-right">
                            @if(Auth::guard('candidate')->check())
                                <li class="left"><a href="#">{{ Auth::guard('candidate')->user()->name }}</a> </li>
                                <li class="right"><a href="{{ route('website.logout') }}">Logout</a></li>
                            @else

                                <li class="left"><a href="{{  route('website.registration') }}"><i class="ti-pencil-alt"></i> Register</a></li>
                                <li class="right"><a  href="{{ route('website.login') }}"><i class="ti-lock"></i>  Log In</a></li>
                            @endif   </ul>
                    </div>
                </div>

                <!-- Mobile Menu Start -->
                <ul class="wpb-mobile-menu">
                    <li>
                        <a @if (Request::is('/')) class="active" @endif  href="{{ route('website.home') }}">Home</a>
                    </li>
                    <li>
                        <a @if (Request::is('about') || Request::is('cms/job-detail')) class="active" @endif href="{{ route('website.about') }}"> About</a>
                        <ul>
                            <li><a @if (Request::is('about')) class="active" @endif href="{{ route('website.about') }}">About</a></li>
                            <li><a @if(Request::is('cms/job-detail')) class="active" @endif href="{{ route('website.cms',['id' => 'job-detail']) }}">Job Detail</a></li>
                        </ul>
                    </li>
                    <li>
                        <a @if (Request::is('browse/jobs') || Request::is('browse/categories') || Request::is('job/alerts') || Request::is('take/appointment')) class="active" @endif href="#"> Candidates</a>
                        <ul>
                            <li><a @if (Request::is('browse/jobs')) class="active" @endif href="{{ route('website.browseJobs') }}">Browse Jobs</a></li>
                            <li><a @if(Request::is('browse/categories')) class="active" @endif href="{{ route('website.browseCategories') }}">Browse Categories</a></li>
                            @if(Auth::guard('candidate')->check())
                            <li><a @if(Request::is('take/appointment')) class="active" @endif href="{{ route('website.takeAppointment') }}">Take Appointment</a></li>
                            <li><a @if(Request::is('book/interview')) class="active" @endif href="{{ route('website.bookInterview') }}">Book Interview</a></li>
                            @endif
                            <li><a @if(Request::is('job/alerts')) class="active" @endif href="{{ route('website.jobAlerts') }}">Job Alerts</a></li>
                        </ul>
                    </li>
                    <li>
                        <a @if (Request::is('contact')) class="active" @endif href="{{ route('website.contact') }}">Contact</a>
                    </li>
                    <li>
                        <a @if (Request::is('enquiry')) class="active" @endif href="{{ route('website.enquiry') }}">Enquiry</a>
                    </li>

                    @if(Auth::guard('candidate')->check())
                        <li class="btn-m"><a href="{{ route('website.logout') }}"><i class="ti-lock"></i> Logout</a></li>
                    @else
                        <li class="btn-m"><a href="{{ route('website.registration') }}"><i class="ti-pencil-alt"></i> Register</a></li>
                        <li class="btn-m"><a href="{{ route('website.login') }}"><i class="ti-lock"></i>  Log In</a></li>
                    @endif
                </ul>
                <!-- Mobile Menu End -->

            </nav>

            <!-- Off Canvas Navigation -->
            <div class="navmenu navmenu-default navmenu-fixed-left offcanvas">
                <!--- Off Canvas Side Menu -->
                <div class="close" data-toggle="offcanvas" data-target=".navmenu">
                    <i class="ti-close"></i>
                </div>
                <h3 class="title-menu">All Pages</h3>
                <ul class="nav navmenu-nav">
                    <li><a href="{{ route('website.home') }}">Home</a></li>
                    <li><a href="{{ route('website.about') }}">About</a></li>
                    <li><a href="{{ route('website.cms',['id' => 'job-detail']) }}">Job Detail</a></li>
                    <li><a href="{{ route('website.browseJobs') }}">Browse Jobs</a></li>
                    <li><a href="{{ route('website.browseCategories') }}">Browse Categories</a></li>
                    @if(Auth::guard('candidate')->check())
                    <li><a href="{{ route('website.takeAppointment') }}">Take Appointment</a></li>
                    @endif
                    <li><a href="{{ route('website.jobAlerts') }}">Job Alerts</a></li>
                    <li><a href="{{ route('website.contact') }}">Contact</a></li>
                    <li><a href="{{ route('website.enquiry') }}">Enquiry</a></li>
                    @if(Auth::guard('candidate')->check())
                        <li><a href="{{ route('website.logout') }}">Logout</a></li>
                    @else
                        <li><a href="{{ route('website.registration') }}"> Register</a></li>
                        <li><a href="{{ route('website.login') }}"> Log In</a></li>
                    @endif
                </ul><!--- End Menu -->
            </div> <!--- End Off Canvas Side Menu -->
            <div class="tbtn wow pulse" id="menu" data-wow-iteration="infinite" data-wow-duration="500ms" data-toggle="offcanvas" data-target=".navmenu">
                <p><i class="ti-files"></i> All Pages</p>
            </div>

        </div>

        <!-- Header Section End -->


    @yield('content')


    <!-- Footer Section Start -->
        <footer>
            <!-- Footer Area Start -->
            <section class="footer-Content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="widget">
                                <h3 class="block-title"><img width="100px" height="50px" src="{{ asset('assets/img/amigos - divyang.png') }}" class="img-responsive" alt="Footer Logo"></h3>
                                <div class="textwidget">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis. Etiam euismod ornare elementum. Sed ex est, consectetur eget facilisis sed.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="widget">
                                <h3 class="block-title">Quick Links</h3>
                                <ul class="menu">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Support</a></li>
                                    <li><a href="#">License</a></li>
                                    <li><a href="#">Terms & Conditions</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="widget">
                                <h3 class="block-title">Trending Jobs</h3>
                                <ul class="menu">
                                    <li><a href="#">Android Developer</a></li>
                                    <li><a href="#">Senior Accountant</a></li>
                                    <li><a href="#">Frontend Developer</a></li>
                                    <li><a href="#">Junior Tester</a></li>
                                    <li><a href="#">Project Manager</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="widget">
                                <h3 class="block-title">Follow Us</h3>
                                <div class="bottom-social-icons social-icon">
                                    <a class="twitter" href="https://twitter.com/GrayGrids"><i class="ti-twitter-alt"></i></a>
                                    <a class="facebook" href="https://web.facebook.com/GrayGrids"><i class="ti-facebook"></i></a>
                                    <a class="youtube" href="https://youtube.com"><i class="ti-youtube"></i></a>
                                    <a class="dribble" href="https://dribbble.com/GrayGrids"><i class="ti-dribbble"></i></a>
                                    <a class="linkedin" href="https://www.linkedin.com/GrayGrids"><i class="ti-linkedin"></i></a>
                                </div>
                                <p>Join our mailing list to stay up to date and get notices about our new releases!</p>
                                <form class="subscribe-box">
                                    <input type="text" placeholder="Your email">
                                    <input type="submit" class="btn-system" value="Send">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Footer area End -->

            <!-- Copyright Start  -->
            <div id="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="site-info text-center">
                                <p>All Rights reserved &copy; 2017 - Designed & Developed by <a rel="nofollow" href="http://www.mtajsolutions.com/">MTAJ Solutions</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Copyright End -->

        </footer>
        <!-- Footer Section End -->

        <!-- Go To Top Link -->
        <a href="#" class="back-to-top">
            <i class="ti-arrow-up"></i>
        </a>

        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_one"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_three"></div>
                    <div class="object" id="object_four"></div>
                    <div class="object" id="object_five"></div>
                    <div class="object" id="object_six"></div>
                    <div class="object" id="object_seven"></div>
                    <div class="object" id="object_eight"></div>
                </div>
            </div>
        </div>

        <!-- Main JS  -->
        <script type="text/javascript" src="{{ asset('assets/js/jquery-min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/material.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/material-kit.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.parallax.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.slicknav.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/main.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.counterup.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/waypoints.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jasny-bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap-select.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/form-validator.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/contact-form-script.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.themepunch.revolution.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.themepunch.tools.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/sweetalert/sweetalert2.js') }}"></script>

        @if(Session::has('message'))
            <script>
                swal({
                    title: 'Success!',
                    text: '{{ Session::get('message') }}',
                    type: 'success',
                    confirmButtonText: 'Ok'
                })
            </script>
        @endif

        @if(Session::has('error'))
            <script>
                swal({
                    title: 'Oops!',
                    text: '{{ Session::get('error') }}',
                    type: 'error',
                    confirmButtonText: 'Ok'
                })
            </script>
        @endif

        @if(Session::has('unauthorized'))
            <script>
                swal('Unauthorized!', 'You don\'t have enough permissions.', 'warning');
            </script>
        @endif
</body>
</html>