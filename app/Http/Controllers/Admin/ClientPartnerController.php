<?php

namespace App\Http\Controllers\Admin;

use App\ClientPartner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientPartnerController extends Controller
{
    // Client Partner dashboard
    public function index(){
        $partners = ClientPartner::get();
        return view('admin.clientpartner.index',[ 'partners' => $partners ]);
    }

    //add cms page
    public function addClientPartner(){
        return view('admin.clientpartner.addclientpartner');
    }

    public function storeClientPartner(Request $request){

        $this->validate($request ,array(
            'image' => 'required'
        ));

        if($request->file('image') != ""){
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = base_path('/assets/front/clientspartners');
            $image->move($destinationPath, $input['imagename']);
        } else {
            $input['imagename'] = "";
        }

        $clientPartners = ClientPartner::create([
            'image' => $input['imagename'],
            'link' => $request->link
        ]);

        $clientPartners->save();
        return redirect(route('admin.clientPartners'))->with('message','Partner details have been saved successfully.');
    }

    public function editClientPartner($id){
        $partner = ClientPartner::find($id);
        return view('admin.clientpartner.editclientpartner',['partner'=>$partner]);
    }

    public function updateClientPartner(Request $request ,$id){

        if($request->file('image')!=""){
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = base_path('/assets/front/clientspartners');
            $image->move($destinationPath, $input['imagename']);
        } else {
            $input['imagename'] = $request['hiddenImage'];
        }

        $partner = ClientPartner::find($id);

        $partner->image = $input['imagename'];
        $partner->link = $request->link;
        $partner->save();

        return redirect(route('admin.clientPartners'))->with('message', 'Partner details has been updated Successfully.');
    }

    // delete category
    public function deleteClientPartner(Request $request)
    {
        $this->validate($request, [
            'partnerId' => 'required|exists:client_partners,id'
        ]);
        $partner =ClientPartner::find($request->partnerId);
        $partner->delete();
    }

}
