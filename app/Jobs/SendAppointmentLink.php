<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendAppointmentLink implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $appointment, $pdfPath;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($appointment ,$pdfPath)
    {
        $this->appointment = $appointment;
        $this->pdfPath = $pdfPath;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $template_path = 'email-template.appointment';

        Mail::send($template_path, ['appointment'=>$this->appointment] , function($message)  {
            // Set the receiver and subject of the mail.
            $message->to($this->appointment->candidate->email, '')->subject('Appointment Letter : Job Board');
            // Set the sender
            $message->from(env('MAIL_USERNAME'),'MTAJ Solutions');

            $message->attach($this->pdfPath);
        });
    }
}
