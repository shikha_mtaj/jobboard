<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Candidate;
use App\CandidateDocument;
use App\CandidateExperience;
use App\CandidateJob;
use App\CandidateQualification;
use App\CandidateResult;
use App\Category;
use App\JobLocation;
use App\Jobpost;
use App\Jobs\SendAppointmentLink;
use App\Jobs\SendCandidateApprovalLink;
use App\Jobs\SendCandidateJobApprovalLink;
use App\Jobs\SendCandidateRejectLink;
use PDF;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class CandidateController extends Controller
{
    // candidate listing
    public function index(){

        $candidates = Candidate::get();
        return view('admin.candidate.candidates',['candidates' => $candidates]);

    }

    // delete candidate
    public function deleteCandidate(Request $request)
    {
        $this->validate($request, [
            'candidateId' => 'required|exists:candidates,id'
        ]);
        $candidate =Candidate::find($request->candidateId);
        $candidate->delete();
    }

    // view candidate details
    public function viewCandidate($id){

        $candidate = Candidate::where('id',$id)->first();
        $candidate_documents = CandidateDocument::where('candidate_id',$id)->first();
        $candidate_qualifications = CandidateQualification::where('candidate_id',$id)->get();
        $candidate_experiences = CandidateExperience::where('candidate_id',$id)->get();
        return view('admin.candidate.viewcandidate',[ 'candidate' => $candidate, 'candidate_documents' => $candidate_documents ,'candidate_qualifications' => $candidate_qualifications ,'candidate_experiences' => $candidate_experiences ] );

    }

    // candidate status change
    public function changeStatus(Request $request)
    {
        $this->validate($request, [
            'candidateId' => 'required|exists:candidates,id'
        ]);

        $candidate = Candidate::find($request['candidateId']);
        if ($request['status']) { $candidate->status = '0'; }
        else { $candidate->status = '1'; }

        $candidate->save();

        if($request['status'] == '0'){

        /*$response = (new \GuzzleHttp\Client)->request('POST', 'http://www.smsidea.co.in/sendsms.aspx', [
            'form_params' => [
                'mobile' => '9512018399',
                'pass' => 'jigar#1234',
                'senderid'=>'SMSWEB',
                'to'=>$candidate->phone,
                'msg'=>'Dear '.$candidate->name.', Your Registration has been approved. You can login on the website throug your credentials.'
            ]
        ]);
        $this->dispatch(new SendCandidateApprovalLink($candidate));*/

        } else {

        /*$response = (new \GuzzleHttp\Client)->request('POST', 'http://www.smsidea.co.in/sendsms.aspx', [
                'form_params' => [
                    'mobile' => '9512018399',
                    'pass' => 'jigar#1234',
                    'senderid'=>'SMSWEB',
                    'to'=>$candidate->phone,
                    'msg'=>"Dear ".$candidate->name.", Your account has been deactivated. Please contact Admin."
                ]
        ]);
        $this->dispatch(new SendCandidateRejectLink($candidate));*/

        }

        return response()->json([
            'status' => $request['status'] ? 0 : 1
        ]);
    }

    // candidate jobs
    public function jobs($id){
        $candidatejobs = CandidateJob::with('jobDetail')->where('candidate_id',$id)->get();
        return view('admin.candidate.jobs',['candidatejobs' => $candidatejobs]);
    }

    // view Candidate Job Result
    public function viewCandidateJobResult($id){

        $candidateJob = CandidateJob::find($id);
        $candidateJobResult = CandidateResult::where(['candidate_id' => $candidateJob->candidate_id ,'jobpost_id' => $candidateJob->jobpost_id])->first();

        return view('admin.candidate.viewjobresult',['candidateJobResult' => $candidateJobResult]);
    }

    // view candidate appointments
    public function appointments($id){

        $appointments = Appointment::where(['candidate_id' => $id])->get();

        foreach ($appointments AS $key => $appointment){
            $categories = Category::whereIn('id',explode(',',$appointment->category_ids))->pluck('name')->toArray();
            $joblocations = JobLocation::whereIn('id',explode(',',$appointment->joblocation_ids))->pluck('name')->toArray();
            $appointments[$key]['categories'] = implode(',',$categories);
            $appointments[$key]['joblocations'] = implode(',',$joblocations);
        }

        return view('admin.candidate.appointments',[ 'appointments' => $appointments ]);
    }

    // delete appointment
    public function deleteAppointment(Request $request)
    {
        $this->validate($request, [
            'appointmentId' => 'required|exists:appointments,id'
        ]);
        $appointment =Appointment::find($request->appointmentId);
        $appointment->delete();
    }

    // Approve candidate appointments
    public function approveCandidateAppointments(Request $request)
    {
        if($request['appointments'] != ""){
            $appointments = Appointment::wherein('id',$request['appointments'])->get();

            foreach ($appointments AS $key => $appointment){
                $categories = Category::whereIn('id',explode(',',$appointment->category_ids))->pluck('name')->toArray();
                $joblocations = JobLocation::whereIn('id',explode(',',$appointment->joblocation_ids))->pluck('name')->toArray();
                $appointments[$key]['categories'] = implode(',',$categories);
                $appointments[$key]['joblocations'] = implode(',',$joblocations);
            }

            foreach ($appointments AS $key => $appointment){

                $data = array('appointment' => $appointment);

                $pdfName = str_replace(' ','',$appointment->candidate->name) . time();
                $pdfPath = base_path('assets/front/appointment/' . $pdfName . '.pdf');
                PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
                PDF::loadView('invoice', $data)->save($pdfPath);

                $this->dispatch(new SendAppointmentLink($appointment ,$pdfPath));

                Appointment::where(['id' => $appointment->id])->update(['status' => 1]);

            }
        }
    }

    public function viewAppointment($id){

        $appointment = Appointment::where('id',$id)->first();

        $categories = Category::whereIn('id',explode(',',$appointment->category_ids))->pluck('name')->toArray();
        $joblocations = JobLocation::whereIn('id',explode(',',$appointment->joblocation_ids))->pluck('name')->toArray();
        $appointment['categories'] = implode(',',$categories);
        $appointment['joblocations'] = implode(',',$joblocations);

        return view('admin.candidate.viewappointment',['appointment' => $appointment]);

    }



}
