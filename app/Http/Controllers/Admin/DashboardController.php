<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Candidate;
use App\Jobpost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function index(Request $request)
    {
        $total_jobs = Jobpost::count();
        $total_candidates = Candidate::count();
        $total_appointments = Appointment::count();

        return view('admin.dashboard',['total_jobs' => $total_jobs ,'total_candidates'=>$total_candidates ,'total_appointments'=>$total_appointments]);
    }

}
