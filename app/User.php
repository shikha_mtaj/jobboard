<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{

    use Notifiable;
    const userType = 1;


    protected $fillable = [
        'name', 'email', 'password','mobile'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeOperator($q)
    {
        return $q->whereUserType(self::userType);
    }

}
