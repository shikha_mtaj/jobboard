
<div>
    Hello Admin,<br><br>

    Enquiry from jobboard by {{ $enquiry->name }} <br><br>

    <b>Name :</b> {{ $enquiry->name }} <br><br>

    <b>Email ID :</b> {{ $enquiry->email }} <br><br>

    <b>Phone No :</b> {{ $enquiry->phone }} <br><br>

    <b>Message :</b> {{ $enquiry->message }} <br><br>

    Regards,<br>
    Job Board
</div>
