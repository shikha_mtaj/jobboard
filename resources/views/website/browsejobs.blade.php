@extends('website.pageLayout')

@section('content')

    <!-- Page Header Start -->
    <div class="page-header" style="background: url({{ URL::asset('assets/img/banner1.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Browse Job</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('website.home') }}"><i class="ti-home"></i> Home</a></li>
                            <li class="current">Browse Job</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <!-- Job Browse Section Start -->
    <section class="job-browse section">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    @if(isset($jobs[0]) && !empty($jobs[0]))
                    @foreach($jobs as $job)
                    <div class="job-list">
                        <div class="thumb">
                            <a href="{{ route('website.job_details',['id'=>$job->id]) }}"><img style="width:100px" src="{{ asset('assets/images/job/'.$job->image)}}" alt=""></a>
                        </div>
                        <div class="job-list-content">
                            <h4><a href="{{ route('website.job_details',['id'=>$job->id])}}">{{$job->job_title}}</a><span class="full-time">{{$job->job_type}}</span></h4>
                            <p>{!! $job->short_description !!}</p>
                            <div class="job-tag">
                                <div class="pull-left">
                                    <div class="meta-tag">
                                        <span><a href="#"><i class="ti-desktop"></i>{{ $job->category->name }}</a></span>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('website.job_details',['id'=>$job->id]) }}" class="btn btn-common btn-rm">More Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                        <div class="job-list">
                            <h4>No Jobs Available.</h4>
                        </div>
                    @endif



                </div>
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <div class="sidebar">
                            <div class="inner-box">
                                <h3>Categories</h3>
                                <ul class="cat-list">
                                    @foreach($categories AS $category)
                                    <li>
                                        <a href="{{ route('website.browseCategoryJobs',['id' => $category->id]) }}">{{ $category->name }} <span class="num-posts"> : {{ $category->categoryJobs()->count() }}  Jobs</span></a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
    <!-- Job Browse Section End -->


@endsection