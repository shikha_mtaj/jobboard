<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToCandidateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('candidate_jobs', function(Blueprint $table)
        {
            $table->foreign('candidate_id', 'candidate_jobs_fk0')->references('id')->on('candidates')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('jobpost_id', 'candidate_jobs_fk1')->references('id')->on('jobposts')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('candidate_jobs', function(Blueprint $table)
        {
            $table->dropForeign('candidate_jobs_fk0');
            $table->dropForeign('candidate_jobs_fk1');
        });
    }
}
