@extends('website.pageLayout')

@section('content')
    <!-- Page Header Start -->
    <div class="page-header" style="background: url({{ URL::asset('assets/img/banner1.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">{{ $cms->title }}</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('website.home') }}"><i class="ti-home"></i> Home</a></li>
                            <li class="current">{{ $cms->title }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <!-- Main container Start -->
    <div class="about section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="about-content">
                        <h2 class="medium-title">{{ $cms->title }}</h2>
                        {!! $cms->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main container End -->

@endsection