<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([
    'namespace' => 'Website', // it is added in controller function name
    'prefix' => '', //it is added in url
    'as' => 'website.' //route name
], function () {
    Route::get('', 'HomeController@index')->name('home');
    Route::get('about', 'AboutController@index')->name('about');
    Route::get('contact', 'ContactController@index')->name('contact');
    Route::post('contact/email','ContactController@contactEmail')->name('contactEmail');

    Route::get('login', 'LoginController@index')->name('login');
    Route::get('job_details/{id}', 'JobController@index')->name('job_details');

    Route::get('login','LoginController@index')->name('login');
    Route::get('registration', 'RegistrationController@index')->name('registration');
    Route::post('store/candidate','RegistrationController@storeCandidate')->name('storeCandidate');
    Route::post('authenticate','LoginController@authenticate')->name('authenticate');
    Route::get('varify/otp','RegistrationController@otpForm')->name('otpForm');
    Route::post('varify/otp/success','RegistrationController@varifyOtp')->name('varifyOtp');

    // Search Section
    Route::post('view/searchjobs','SearchController@index')->name('searchResult');
    Route::get('view/searchjobs/{title}/{category_id}','SearchController@searchResult')->name('viewSearchResult');

    // Cms
    Route::get('cms/{id}','CmsController@page')->name('cms');

    // Enquiry Section
    Route::get('enquiry','EnquiryController@index')->name('enquiry');
    Route::post('store/enquiry','EnquiryController@storeEnquiry')->name('storeEnquiry');

    // Browse Jobs
    Route::get('browse/jobs','BrowseJobController@index')->name('browseJobs');

    // Browse Categories
    Route::get('browse/categories','BrowseCategoryController@index')->name('browseCategories');
    Route::get('browse/category/jobs/{id}','BrowseCategoryController@categoryJobs')->name('browseCategoryJobs');
    // Job Alert
    Route::get('job/alerts','JobAlertController@index')->name('jobAlerts');

    Route::group(['middleware' => 'candidate.auth'], function () {

      Route::get('logout', 'LoginController@logout')->name('logout');
      Route::get('apply/job/{id}', 'JobController@applyJob')->name('applyJob');

      Route::get('take/appointment','AppointmentController@index')->name('takeAppointment');
      Route::post('get/city/partners','AppointmentController@getCityPartners')->name('getCityPartners');
      Route::post('store/appointment','AppointmentController@storeAppointment')->name('storeAppointment');

      Route::get('book/interview','BookInterviewController@index')->name('bookInterview');
      Route::post('get/category/jobs','BookInterviewController@getCategoryJobs')->name('getCategoryJobs');
      Route::post('store/book/interview','BookInterviewController@storeBookInterview')->name('storeBookInterview');

    });
});



Route::group([
    'namespace' => 'Admin', // it is added in controller function name
    'prefix' => 'admin', //it is added in url
    'as' => 'admin.' //route name
], function () {
    Route::get('','LoginController@loginForm');
    Route::post('authenticate', 'LoginController@authenticate')->name('authenticate');

    Route::get('forgot/password', 'ForgotPasswordController@forgotPasswordEmail')->name('forgotPassword');
    Route::post('verify/email', 'ForgotPasswordController@verifyForgotPasswordEmail')->name('verifyEmail');
    Route::get('reset/password/{token}', 'ForgotPasswordController@resetPasswordForm')->name('resetPasswordForm');
    Route::post('reset/password/success', 'ForgotPasswordController@resetPassword')->name('resetPassword');


    Route::group(['middleware' => 'admin.auth'], function () {

        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('logout','LoginController@logout')->name('logout');

        Route::get('profile','ProfileController@profile')->name('profile');
        Route::post('update/Profile','ProfileController@updateProfile')->name('updateProfile');

        Route::get('change/password','ChangePasswordController@changePasswordForm')->name('changePasswordForm');
        Route::post('update/password','ChangePasswordController@updatePassword')->name('updatePassword');

        // job posts
        Route::get('job', 'JobPostController@index')->name('job');
        Route::get('add/job', 'JobPostController@addJob')->name('addJob');
        Route::post('store/job', 'JobPostController@storeJob')->name('storeJob');
        Route::get('edit/job/{id}', 'JobPostController@editJob')->name('editJob');
        Route::post('update/job/{id}', 'JobPostController@updateJob')->name('updateJob');
        Route::post('delete/job', 'JobPostController@deleteJob')->name('deleteJob');
        //Route::post('delete/all/jobs', 'JobPostController@deleteAll')->name('deleteAll')->middleware('permission:delete.job');
        Route::get('view/job/candidates/{id}','JobPostController@viewJobCandidates')->name('viewJobCandidates');
        Route::post('sendmail/job/candidates', 'JobPostController@approveJobCandidate')->name('approveJobCandidate');
        Route::post('delete/job/candidates', 'JobPostController@deleteJobCandidate')->name('deleteJobCandidate');
        Route::get('job/candidateresult/{id}','JobPostController@viewJobCandidateResult')->name('viewJobCandidateResult');
        Route::post('changestatus/job', 'JobPostController@changeStatus')->name('changeStatusJob');

        Route::get('upload/job/excel','JobPostController@uploadJobsExcel')->name('uploadJobsExcel');
        Route::post('store/job/excel','JobPostController@storeJobsExcel')->name('storeJobsExcel');

        Route::get('view/job/openings/{id}', 'JobPostController@viewJobOpenings')->name('viewJobOpenings');
        Route::get('add/job/opening/{id}', 'JobPostController@addJobOpening')->name('addJobOpening');
        Route::post('delete/job/opening', 'JobPostController@deleteJobOpening')->name('deleteJobOpening');
        Route::post('store/job/opening', 'JobPostController@storeJobOpening')->name('storeJobOpening');

        //candidate section
        Route::get('candidate','CandidateController@index')->name('candidate');
        Route::post('delete/candidate', 'CandidateController@deleteCandidate')->name('deleteCandidate');
        Route::get('view/candidate/{id}','CandidateController@viewCandidate')->name('viewCandidate');
        Route::post('changestatus/candidate', 'CandidateController@changeStatus')->name('changeStatusCandidate');

        Route::get('candidate/jobs/{id}','CandidateController@jobs')->name('viewCandidateJobs');
        Route::post('delete/candidate/job', 'CandidateController@deleteCandidateJob')->name('deleteCandidateJob');
        Route::post('sendmail/candidate/job', 'CandidateController@approveCandidateJob')->name('approveCandidateJob');
        Route::get('candidate/jobresult/{id}','CandidateController@viewCandidateJobResult')->name('viewCandidateJobResult');

        Route::get('candidate/appointments/{id}','CandidateController@appointments')->name('viewCandidateAppointments');
        Route::post('delete/candidate/appointment', 'CandidateController@deleteAppointment')->name('deleteCandidateAppointment');
        Route::post('approve/candidate/appointments','CandidateController@approveCandidateAppointments')->name('approveCandidateAppointments');
        Route::get('view/appointment/{id}','CandidateController@viewAppointment')->name('viewAppointment');

        //Cms
        Route::get('cms', 'CmsController@index')->name('cms');
        Route::get('add/cms', 'CmsController@addCms')->name('addCms');
        Route::post('store/cms', 'CmsController@storeCms')->name('storeCms');
        Route::get('edit/cms/{id}', 'CmsController@editCms')->name('editCms');
        Route::post('update/cms/{id}', 'CmsController@updateCms')->name('updateCms');

        //Categories
        Route::get('category', 'CategoryController@index')->name('category');
        Route::get('add/category', 'CategoryController@addCategory')->name('addCategory');
        Route::post('store/category', 'CategoryController@storeCategory')->name('storeCategory');
        Route::get('edit/category/{id}', 'CategoryController@editCategory')->name('editCategory');
        Route::post('update/category/{id}', 'CategoryController@updateCategory')->name('updateCategory');
        Route::post('delete/category', 'CategoryController@deleteCategory')->name('deleteCategory');
        Route::post('changestatus/category', 'CategoryController@changeStatus')->name('changeStatusCategory');

        //Enquiry
        Route::get('enquiry','EnquiryController@index')->name('enquiry');
        Route::post('delete/enquiry', 'EnquiryController@deleteEnquiry')->name('deleteEnquiry');

        //City
        Route::get('city','CityController@index')->name('city');
        Route::post('delete/city', 'CityController@deleteCity')->name('deleteCity');
        Route::post('delete/all/cities', 'CityController@deleteAllCities')->name('deleteAllCities');
        Route::get('upload/city/excel','CityController@uploadCitiesExcel')->name('uploadCitiesExcel');
        Route::post('store/city/excel','CityController@storeCitiesExcel')->name('storeCitiesExcel');

        Route::get('view/city/partners/{id}', 'CityController@viewCityPartners')->name('viewCityPartners');
        Route::get('add/city/partner/{id}', 'CityController@addCityPartner')->name('addCityPartner');
        Route::post('delete/city/partner', 'CityController@deleteCityPartner')->name('deleteCityPartner');
        Route::post('store/city/partner', 'CityController@storeCityPartner')->name('storeCityPartner');

        //Job Locations
        Route::get('joblocation','JobLocationController@index')->name('jobLocation');
        Route::post('delete/joblocation', 'JobLocationController@deleteJobLocation')->name('deleteJobLocation');
        Route::post('delete/all/joblocations', 'JobLocationController@deleteAllJobLocations')->name('deleteAllJobLocations');
        Route::get('upload/joblocation/excel','JobLocationController@uploadJobLocationsExcel')->name('uploadJobLocationsExcel');
        Route::post('store/joblocation/excel','JobLocationController@storeJobLocationsExcel')->name('storeJobLocationsExcel');

        //Appointment
        Route::get('appointment/{id}','AppointmentController@index')->name('appointment');
        Route::post('delete/appointment', 'AppointmentController@deleteAppointment')->name('deleteAppointment');
        Route::post('approve/appointments','AppointmentController@approveAppointments')->name('approveAppointments');
        Route::get('view/appointment/{id}','AppointmentController@viewAppointment')->name('viewAppointment');

        //Clients & Partners
        Route::get('partner','ClientPartnerController@index')->name('clientPartners');
        Route::get('add/partner', 'ClientPartnerController@addClientPartner')->name('addClientPartner');
        Route::post('store/partner', 'ClientPartnerController@storeClientPartner')->name('storeClientPartner');
        Route::get('edit/partner/{id}', 'ClientPartnerController@editClientPartner')->name('editClientPartner');
        Route::post('update/partner/{id}', 'ClientPartnerController@updateClientPartner')->name('updateClientPartner');
        Route::post('delete/partner', 'ClientPartnerController@deleteClientPartner')->name('deleteClientPartner');

        //Clients & Partners
        Route::get('testimonial','TestimonialController@index')->name('testimonial');
        Route::get('add/testimonial', 'TestimonialController@addTestimonial')->name('addTestimonial');
        Route::post('store/testimonial', 'TestimonialController@storeTestimonial')->name('storeTestimonial');
        Route::get('edit/testimonial/{id}', 'TestimonialController@editTestimonial')->name('editTestimonial');
        Route::post('update/testimonial/{id}', 'TestimonialController@updateTestimonial')->name('updateTestimonial');
        Route::post('delete/testimonial', 'TestimonialController@deleteTestimonial')->name('deleteTestimonial');

    });
});




