@extends('mainlayout')

@section('title')
    Candidate Result
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#">
                                    <i class="icon-bell"></i> Action</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-shield"></i> Another action</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-user"></i> Something else here</a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="#">
                                    <i class="icon-bag"></i> Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> {{ $candidateJobResult->candidateDetails->name }} </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="profile">
                <div class="tabbable-line tabbable-full-width">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" data-toggle="tab"> Result Details </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="list-unstyled profile-nav">
                                        <li>
                                            @if($candidateJobResult->jobDetails->image)
                                                <img src="{{ asset('assets/images/job/'.$candidateJobResult->jobDetails->image) }}" class="img-responsive pic-bordered" alt="" />
                                            @else
                                                <img src="{{ asset('assets/img/no-image.png') }}" class="img-responsive pic-bordered" alt="" />
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-8 profile-info">
                                            <h1 class="font-green sbold uppercase">{{ $candidateJobResult->jobDetails->job_title }}</h1>
                                            @if(!empty($candidateJobResult->jobDetails->designation))
                                                <p><strong>Designation :</strong> {{ $candidateJobResult->jobDetails->designation }}</p>
                                            @endif
                                            {!! $candidateJobResult->jobDetails->job_description !!}
                                        </div>
                                        <!--end col-md-8-->
                                    </div>
                                    <!--end row-->
                                    <div class="tabbable-line tabbable-custom-profile">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1_11">

                                                <div class="portlet-body">
                                                    <table class="table table-striped table-bordered table-advance table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>
                                                                <i class="fa fa-briefcase"></i> Name </th>
                                                            <th class="hidden-xs">
                                                                <i class="fa fa-question"></i> Description </th>
                                                            <th> </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><a href="javascript:;"> Total Questions </a></td>
                                                            <td class="hidden-xs"> {{ $candidateJobResult->total_questions }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Correct Answers </a></td>
                                                            <td class="hidden-xs"> {{ $candidateJobResult->correct_answers }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Percentage </a></td>
                                                            <td class="hidden-xs"> {{ round($candidateJobResult->percentage,2) }} %</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Status </a></td>
                                                            <td class="hidden-xs"> {{ $candidateJobResult->status }} </td>
                                                            <td></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                            <!--tab-pane-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@endsection

@section('js')

@endsection