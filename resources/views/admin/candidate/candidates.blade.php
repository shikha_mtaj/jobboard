@extends('mainlayout')

@section('title')
    Candidates
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Candidates</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>
            <img src="{{ asset('assets/loader.gif') }}" style="display: none;" class="loading-img" width="100px" height="100px">
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="portlet light bordered">
                    <div class="portlet-title"><div class="tools"></div></div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                            <tr>
                                <th>Sr. No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile Number</th>
                                <th>Resume</th>
                                <th width="45px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($candidates as $key=>$candidate)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $candidate->name }}</td>
                                    <td>{{ $candidate->email }}</td>
                                    <td>{{ $candidate->mobile }}</td>
                                    <td>
                                        @if(!empty($candidate->resume))
                                            <a href="{{ asset('assets/front/candidate_documents/'.$candidate->resume) }}" download="">Download</a>
                                        @endif
                                    </td>
                                    <td>
											<div class="btn-group">
                                                <a class="btn btn-xs green dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true"> Actions
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="{{ route('admin.viewCandidateAppointments',['id' => $candidate->id]) }}">
                                                            <i class="fa fa-pencil"></i> Appointments </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('admin.viewCandidateJobs',['id' => $candidate->id]) }}">
                                                            <i class="fa fa-pencil"></i> Jobs </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" class="deleteCandidate" id="deleteCandidate" data-val="{{ $candidate->id }}">
                                                            <i class="fa fa-trash-o"></i> Delete </a>
                                                    </li>
                                                   
                                                </ul>
                                            </div>
									</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

@endsection

@section('js')

    <script type="text/javascript">


        $('.status_check').click(function () {
            var status = parseInt($(this).val());
            var candidateId = $(this).attr('data-val');

            swal({
                title: 'Are you sure?',
                text: "You want to " + (status ? 'De-Activate' : 'Activate') + " this Candidate!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.changeStatusCandidate') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        status: status,
                        candidateId: candidateId
                    },
                    beforeSend:function(){
                        $('.loading-img').css('display','');
                    },
                    success: function (data) {
                        var element = $('#status_' + candidateId);
                        if (data['status']) {
                            element.html('Active')
                                .removeClass('uk-badge uk-badge-danger')
                                .addClass('uk-badge uk-badge-success');
                        } else {
                            element.html('Inactive')
                                .removeClass('uk-badge uk-badge-success')
                                .addClass('uk-badge uk-badge-danger');
                        }
                        element.val(data['status']);
                        $('.loading-img').css('display','none');
                        swal(
                            (status ? 'De-Activated' : 'Activated') + '!',
                            'Candidate has been ' + (status ? 'De-Activated' : 'Activated') + ' .',
                            "success"
                            );
                    },
                    error: function (xhr, status, error) {
                        swal(
                            'Oops !',
                            'Something went wrong, please try again later.',
                            'warning'
                        );
                    }
                });
            });
        });


        $('.deleteCandidate').on('click', function(e){

            var candidateId = $(this).attr('data-val');

            swal({
                title: 'Are you sure?',
                text: "You want to Delete this Candidate",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.deleteCandidate') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        candidateId: candidateId
                    },
                    success: function (data) {
                        swal({
                            title: 'Success!',
                            text: 'Candidate has been removed Successfully.',
                            type: "success",
                            confirmButtonText: "OK"
                        }).then(function () {
                            location.reload();
                        });
                    },
                    error: function (xhr, status, error) {
                        swal(
                            'Oops !',
                            'Something went wrong, please try again later.',
                            'warning'
                        );
                    }
                });
            });
        });

    </script>

@endsection