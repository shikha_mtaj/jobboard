@extends('mainlayout')

@section('title')
    Add City Partner
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="{{ route('admin.city') }}">City</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Add</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Add New City Partner
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="horizontal-form" method="post" action="{{ route('admin.storeCityPartner') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="city_id" id="city_id" value="{{ $id }}">
                                        <div class="form-body">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                                        <label class="control-label">Name</label>
                                                        <input type="text" name="name" id="name" class="form-control"  value="{{ old('name') }}">
                                                        @if ($errors->has('name'))
                                                            <span class="help-block">{{ $errors->first('name') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('auth_person') ? 'has-error' : '' }}">
                                                        <label class="control-label">Auth Person</label>
                                                        <input type="text" name="auth_person" id="auth_person" class="form-control"  value="{{ old('auth_person') }}">
                                                        @if ($errors->has('auth_person'))
                                                            <span class="help-block">{{ $errors->first('auth_person') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                                                        <label class="control-label">Address</label>
                                                        <input type="text" name="address" id="address" class="form-control"  value="{{ old('address') }}">
                                                        @if ($errors->has('address'))
                                                            <span class="help-block">{{ $errors->first('address') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('contact_no') ? 'has-error' : '' }}">
                                                        <label class="control-label">Contact No</label>
                                                        <input type="text" name="contact_no" id="contact_no" class="form-control"  value="{{ old('contact_no') }}">
                                                        @if ($errors->has('contact_no'))
                                                            <span class="help-block">{{ $errors->first('contact_no') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                                        <label class="control-label">Email ID</label>
                                                        <input type="text" name="email" id="email" class="form-control"  value="{{ old('email') }}">
                                                        @if ($errors->has('email'))
                                                            <span class="help-block">{{ $errors->first('email') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('bonus') ? 'has-error' : '' }}">
                                                        <label class="control-label">Bonus</label>
                                                        <input type="text" name="bonus" id="bonus" class="form-control"  value="{{ old('bonus') }}">
                                                        @if ($errors->has('bonus'))
                                                            <span class="help-block">{{ $errors->first('bonus') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('join_date') ? 'has-error' : '' }}">
                                                        <label class="control-label">Join Date</label>
                                                        <input type="text" data-date-start-date="-{{ date('d') - 1 }}d" data-date="2012-02-12" data-date-format="yyyy-mm-dd" name="join_date" id="join_date" class="form-control date-picker" placeholder="yyyy-mm-dd" value="{{ old('join_date') }}" readonly>
                                                        @if ($errors->has('join_date'))
                                                            <span class="help-block">{{ $errors->first('join_date') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('expiry_date') ? 'has-error' : '' }}">
                                                        <label class="control-label">Expiry Date</label>
                                                        <input type="text" data-date-start-date="-{{ date('d') - 1 }}d" data-date="2012-02-12" data-date-format="yyyy-mm-dd" name="expiry_date" id="expiry_date" class="form-control date-picker" placeholder="yyyy-mm-dd" value="{{ old('expiry_date') }}" readonly>
                                                        @if ($errors->has('expiry_date'))
                                                            <span class="help-block">{{ $errors->first('expiry_date') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <textarea class="ckeditor form-control" name="description" rows="6">{{ old('description') }}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group {{ $errors->has('reference') ? 'has-error' : '' }}">
                                                        <label class="control-label">Reference</label>
                                                        <input type="text" name="reference" id="reference" class="form-control"  value="{{ old('reference') }}">
                                                        @if ($errors->has('reference'))
                                                            <span class="help-block">{{ $errors->first('reference') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <!--/span-->
                                        </div>
                                        <div class="form-actions right">
                                            <a href="{{ route('admin.viewCityPartners',['id' => $id]) }}"><button type="button" class="btn default">Cancel</button></a>
                                            <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection

@section('js')
@endsection