-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 08, 2017 at 02:22 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sos`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `present_address` text COLLATE utf8mb4_unicode_ci,
  `permanent_address` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1:Active, 0:Inactive',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`id`, `name`, `email`, `password`, `gender`, `dob`, `phone`, `age`, `present_address`, `permanent_address`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Manoj Parmar', 'manoj@gmail.com', '$2y$10$JGXX5xxGQshGEKMwYAA7QeiKCJ/2yyXv2kWU5ZDm6PHeODBfXnzlG', 'male', '2017-09-01', '1234567890', 26, 'Baroda', 'Baroda', 0, '3fmu0zcVoT6QGUCvJGAYKFfuGaH2w7K07Rj1RKCjoCXrF1jJjrQ81sXUXe8q', '2017-09-04 03:54:02', '2017-09-08 06:49:53'),
(2, 'Raj', 'raj@gmail.com', '$2y$10$JW6nD4iFSJp1cLhj9jNpouBHF4jSsb6oyQKRnKlmkN7DjTLeMCiTK', 'male', '2017-09-06', '1234567890', 30, 'Baroda', 'Baroda', 1, 'xKbWa3bmXgfWy5LLXSVSNJALk4Q07rv7FsJ4C7Wfc6qB10W41FeO05rJ0SwE', '2017-09-04 04:57:01', '2017-09-06 00:06:33'),
(3, 'Raju Patel', 'rajupatel@gmail.com', '$2y$10$L.KY5sJe9LEoDjDDO5VGzO/eG2WftmmcGp0A9loKH1M73JJ22oKJW', 'male', '2017-09-04', '1212121212', 25, 'Baroda', 'Baroda', 1, 'L3jjmgzi8Wn9KW29zcOWGVi9awmEoZliGo3EjnjnSBnjq8d39gebOdW0rS0I', '2017-09-04 08:31:28', '2017-09-06 01:32:05');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_answers`
--

CREATE TABLE `candidate_answers` (
  `id` int(11) NOT NULL,
  `jobpost_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `candidate_answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `candidate_documents`
--

CREATE TABLE `candidate_documents` (
  `id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ssc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nursery` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_proof` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `adhar_card` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `signature` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidate_documents`
--

INSERT INTO `candidate_documents` (`id`, `candidate_id`, `photo`, `lc`, `ssc`, `nursery`, `address_proof`, `adhar_card`, `signature`, `created_at`, `updated_at`) VALUES
(1, 1, 'photo1504517043.jpg', 'lc1504517043.jpg', 'ssc1504517043.jpg', 'nurserycourse1504517043.jpg', 'addressproof1504517043.jpg', 'adharcard1504517043.jpg', 'signature1504517043.jpg', '2017-09-04 03:54:03', '2017-09-04 03:54:03'),
(2, 2, 'photo1504520822.jpg', 'lc1504520822.jpg', 'ssc1504520822.jpg', 'nurserycourse1504520822.jpg', 'addressproof1504520822.jpg', 'adharcard1504520822.jpg', 'signature1504520822.jpg', '2017-09-04 04:57:02', '2017-09-04 04:57:02'),
(3, 3, 'photo1504533689.jpg', 'lc1504533689.jpg', 'ssc1504533689.jpg', 'nurserycourse1504533689.jpg', 'addressproof1504533689.jpg', 'adharcard1504533689.jpg', 'signature1504533689.jpg', '2017-09-04 08:31:29', '2017-09-04 08:31:29');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_experiences`
--

CREATE TABLE `candidate_experiences` (
  `id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `post_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidate_experiences`
--

INSERT INTO `candidate_experiences` (`id`, `candidate_id`, `post_name`, `organization`, `duration`, `job_description`, `created_at`, `updated_at`) VALUES
(18, 3, 'POST 1', 'ABCD', '5', 'test', '2017-09-06 01:37:14', '2017-09-06 01:37:14'),
(32, 1, 'Post 1', 'ABCD', '3', 'This is just test', '2017-09-07 01:56:04', '2017-09-07 01:56:04'),
(33, 1, 'Post 2', 'xyz', '2 Years', 'This is just test', '2017-09-07 01:56:04', '2017-09-07 01:56:04'),
(37, 2, 'Post 1', 'ABCD', '3', 'This is just test', '2017-09-07 02:03:41', '2017-09-07 02:03:41'),
(38, 2, 'Post 2', 'XYZ', '2 Years', 'This is just test', '2017-09-07 02:03:41', '2017-09-07 02:03:41'),
(39, 2, 'POST 3', 'XXX', '3 Years', 'This is just test', '2017-09-07 02:03:41', '2017-09-07 02:03:41');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_jobs`
--

CREATE TABLE `candidate_jobs` (
  `id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `jobpost_id` int(11) NOT NULL,
  `payment_type` int(11) NOT NULL DEFAULT '0' COMMENT '1:Online, 0:Challan',
  `amount` double DEFAULT NULL,
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '4' COMMENT '1:Success, 2:Failed, 3:Cancelled, 4:Pending',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1:Completed, 0:Pending',
  `flag` int(11) NOT NULL DEFAULT '0' COMMENT '1:Approved,2:Rejected,0:Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidate_jobs`
--

INSERT INTO `candidate_jobs` (`id`, `candidate_id`, `jobpost_id`, `payment_type`, `amount`, `payment_status`, `status`, `flag`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 0, 500, '4', 1, 1, '2017-09-07 01:41:58', '2017-09-07 01:44:20'),
(2, 1, 3, 0, 500, '4', 1, 1, '2017-09-07 01:50:46', '2017-09-07 01:52:17'),
(3, 1, 4, 0, 500, '4', 1, 1, '2017-09-07 01:56:04', '2017-09-07 01:57:33'),
(4, 2, 2, 0, 500, '4', 1, 1, '2017-09-07 02:03:13', '2017-09-07 02:08:05'),
(5, 2, 3, 0, 500, '4', 1, 1, '2017-09-07 02:03:41', '2017-09-07 02:10:23');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_qualifications`
--

CREATE TABLE `candidate_qualifications` (
  `id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `examination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `board` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percentage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidate_qualifications`
--

INSERT INTO `candidate_qualifications` (`id`, `candidate_id`, `examination`, `year`, `board`, `percentage`, `created_at`, `updated_at`) VALUES
(23, 3, '10', '2010', 'GTU', '70', '2017-09-06 01:37:14', '2017-09-06 01:37:14'),
(24, 3, 'Nursary', '2012', 'GTU', '60', '2017-09-06 01:37:14', '2017-09-06 01:37:14'),
(37, 1, '10', '2010', 'GTU', '50', '2017-09-07 01:56:04', '2017-09-07 01:56:04'),
(38, 1, 'Nursing', '2012', 'GTU', '50', '2017-09-07 01:56:04', '2017-09-07 01:56:04'),
(41, 2, '10th', '2010', 'GTU', '50', '2017-09-07 02:03:41', '2017-09-07 02:03:41'),
(42, 2, 'Nursing', '2012', 'GTU', '70', '2017-09-07 02:03:41', '2017-09-07 02:03:41');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_results`
--

CREATE TABLE `candidate_results` (
  `id` int(11) NOT NULL,
  `jobpost_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `total_questions` int(11) NOT NULL,
  `correct_answers` int(11) NOT NULL,
  `percentage` double DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidate_results`
--

INSERT INTO `candidate_results` (`id`, `jobpost_id`, `candidate_id`, `total_questions`, `correct_answers`, `percentage`, `status`, `created_at`, `updated_at`) VALUES
(15, 2, 1, 10, 4, 40, 'FAIL', '2017-09-07 01:44:20', '2017-09-07 01:44:20'),
(16, 3, 1, 10, 5, 50, 'PASS', '2017-09-07 01:52:16', '2017-09-07 01:52:16'),
(17, 4, 1, 10, 4, 40, 'FAIL', '2017-09-07 01:57:33', '2017-09-07 01:57:33'),
(18, 2, 2, 10, 7, 70, 'PASS', '2017-09-07 02:08:05', '2017-09-07 02:08:05'),
(19, 3, 2, 10, 7, 70, 'PASS', '2017-09-07 02:10:22', '2017-09-07 02:10:22');

-- --------------------------------------------------------

--
-- Table structure for table `cms_management`
--

CREATE TABLE `cms_management` (
  `id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keyword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobposts`
--

CREATE TABLE `jobposts` (
  `id` int(11) NOT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_exam_questions` int(11) NOT NULL,
  `job_total_time` double NOT NULL,
  `exam_start_date` date NOT NULL,
  `exam_end_date` date NOT NULL,
  `reg_start_date` date NOT NULL,
  `reg_end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobposts`
--

INSERT INTO `jobposts` (`id`, `job_title`, `job_description`, `image`, `total_exam_questions`, `job_total_time`, `exam_start_date`, `exam_end_date`, `reg_start_date`, `reg_end_date`, `created_at`, `updated_at`) VALUES
(2, 'Air India Recruitment 2017 - 217 Sr. Trainee Pilots & Trainee Pilots', '<p><strong><em>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</em></strong> dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. <u><strong>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</strong></u>, and more recently with desktop publishing software like Aldus PageMaker i<ins><samp><span style="font-family:comic sans ms,cursive">ncluding versions of Lorem Ipsum.</span></samp></ins></p>', NULL, 10, 20, '2017-09-03', '2017-09-20', '2017-09-01', '2017-09-20', '2017-09-04 03:50:47', '2017-09-08 04:24:41'),
(3, 'ASRB Recruitment 2017 - 173 Stenographers & Lower Division Clerks', '<p>Agricultural Scientists Recruitment Board (ASRB) has released a notification for the recruitment of 173 Stenographers and Lower Division Clerks. Interested candidates may check the eligibility criteria and apply online from <strong><span style="font-size:16px">31-08-2017 to 25-09-2017</span>.</strong>&nbsp;</p>\r\n\r\n<p><br />\r\nMore details about ASRB Recruitment (2017), including number of vacancies, eligibility criteria, selection procedure, how to apply and important dates, are given below:</p>\r\n\r\n<p><strong>Vacancy Details:</strong></p>\r\n\r\n<ul>\r\n	<li><strong>Post Name: Stenographer</strong></li>\r\n	<li><strong>No. of Vacancies: 95</strong></li>\r\n	<li><strong>Pay Scale: Rs. 5,200-20,200</strong></li>\r\n	<li><strong>Grade Pay: Rs. 2,400</strong></li>\r\n</ul>\r\n\r\n<p><strong>Job Location: </strong>New Delhi</p>', NULL, 10, 60, '2017-09-01', '2017-09-15', '2017-09-01', '2017-09-15', '2017-09-04 03:51:58', '2017-09-08 04:31:22'),
(4, 'KAR TET Recruitment 2017 - 10,000 Posts of Graduate Primary Teachers', '<p>Karnataka Teacher Eligibility Test (KAR TET) has released a notification for the recruitment of 10,000 Graduate Primary Teachers. Interested candidates may check the eligibility criteria and apply online from<strong> 26-08-2017 to 25-09-2017</strong>.&nbsp;<br />\r\nMore details about KAR TET Recruitment (2017), including number of vacancies, eligibility criteria, selection procedure, how to apply and important dates, are given below:</p>\r\n\r\n<p><strong>Vacancy Details:</strong></p>\r\n\r\n<ul>\r\n	<li><em>Post Name: Graduate Primary Teacher</em></li>\r\n	<li><em>No. of Vacancies: 10,000</em></li>\r\n	<li><em>Pay Scale: Rs. 14,550-26,700</em></li>\r\n</ul>\r\n\r\n<p>Job Location: <strong>Karnataka</strong></p>', '1504522210.jpg', 10, 20, '2017-09-04', '2017-09-12', '2017-09-04', '2017-09-15', '2017-09-04 05:20:10', '2017-09-08 04:27:30');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_questions`
--

CREATE TABLE `job_questions` (
  `id` int(11) NOT NULL,
  `jobpost_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2017_08_21_115639_create_candidate_answers_table', 1),
(3, '2017_08_21_115639_create_candidate_documents_table', 1),
(4, '2017_08_21_115639_create_candidate_results_table', 1),
(5, '2017_08_21_115639_create_candidates_table', 1),
(6, '2017_08_21_115639_create_cms_management_table', 1),
(7, '2017_08_21_115639_create_job_posts_table', 1),
(8, '2017_08_21_115639_create_question_answers_table', 1),
(9, '2017_08_21_115639_create_user_details_table', 1),
(10, '2017_08_21_115639_create_users_table', 1),
(11, '2017_08_21_115640_add_foreign_keys_to_candidate_answers_table', 1),
(12, '2017_08_21_115640_add_foreign_keys_to_candidate_documents_table', 1),
(13, '2017_08_21_115640_add_foreign_keys_to_candidate_results_table', 1),
(14, '2017_08_21_115640_add_foreign_keys_to_user_details_table', 1),
(15, '2017_08_23_095555_create_job_questions_table', 1),
(16, '2017_08_23_100947_add_foreign_keys_to_job_questions_table', 1),
(17, '2017_08_24_060628_create_jobs_table', 1),
(18, '2017_08_25_102635_laratrust_setup_tables', 1),
(19, '2017_08_26_102524_create_candidate_qualifications_table', 1),
(20, '2017_08_26_103159_add_foreign_keys_to_candidate_qualifications_table', 1),
(21, '2017_08_26_103539_create_candidate_experiences_table', 1),
(22, '2017_08_26_103925_add_foreign_keys_to_candidate_experiences_table', 1),
(23, '2017_08_31_105926_create_candidate_jobs_table', 1),
(24, '2017_08_31_111023_add_foreign_keys_to_candidate_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create.job', 'Create Job', 'Permission to create all application job', '2017-09-04 02:17:50', '2017-09-04 02:17:50'),
(2, 'read.job', 'Read Job', 'Permission to read all application job', '2017-09-04 02:17:50', '2017-09-04 02:17:50'),
(3, 'update.job', 'Update Job', 'Permission to update all application job', '2017-09-04 02:17:50', '2017-09-04 02:17:50'),
(4, 'delete.job', 'Delete Job', 'Permission to delete all application job', '2017-09-04 02:17:50', '2017-09-04 02:17:50'),
(5, 'create.operator', 'Create Operator', 'Permission to create all application operator', '2017-09-04 02:17:50', '2017-09-04 02:17:50'),
(6, 'read.operator', 'Read Operator', 'Permission to read all application operator', '2017-09-04 02:17:50', '2017-09-04 02:17:50'),
(7, 'update.operator', 'Update Operator', 'Permission to update all application operator', '2017-09-04 02:17:50', '2017-09-04 02:17:50'),
(8, 'delete.operator', 'Delete Operator', 'Permission to delete all application operator', '2017-09-04 02:17:50', '2017-09-04 02:17:50'),
(9, 'create.upload', 'Create Upload', 'Permission to create all application upload', '2017-09-04 02:17:50', '2017-09-04 02:17:50'),
(10, 'read.candidate', 'Read Candidate', 'Permission to read all application candidate', '2017-09-08 04:17:06', '2017-09-08 04:17:06'),
(11, 'update.candidate', 'Update Candidate', 'Permission to update all application candidate', '2017-09-08 04:17:07', '2017-09-08 04:17:07'),
(12, 'delete.candidate', 'Delete Candidate', 'Permission to delete all application candidate', '2017-09-08 04:17:07', '2017-09-08 04:17:07'),
(13, 'create.report', 'Create Report', 'Permission to create all application report', '2017-09-08 04:17:07', '2017-09-08 04:17:07'),
(14, 'read.report', 'Read Report', 'Permission to read all application report', '2017-09-08 04:17:07', '2017-09-08 04:17:07');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_user`
--

INSERT INTO `permission_user` (`permission_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\User'),
(2, 1, 'App\\User'),
(3, 1, 'App\\User'),
(4, 1, 'App\\User'),
(5, 1, 'App\\User'),
(6, 1, 'App\\User'),
(7, 1, 'App\\User'),
(8, 1, 'App\\User'),
(9, 1, 'App\\User'),
(10, 1, 'App\\User'),
(11, 1, 'App\\User'),
(12, 1, 'App\\User'),
(13, 1, 'App\\User'),
(14, 1, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `question_answers`
--

CREATE TABLE `question_answers` (
  `id` int(11) NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_1` text COLLATE utf8mb4_unicode_ci,
  `answer_2` text COLLATE utf8mb4_unicode_ci,
  `answer_3` text COLLATE utf8mb4_unicode_ci,
  `answer_4` text COLLATE utf8mb4_unicode_ci,
  `true_answer` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_answers`
--

INSERT INTO `question_answers` (`id`, `question`, `answer_1`, `answer_2`, `answer_3`, `answer_4`, `true_answer`, `created_at`, `updated_at`) VALUES
(1, 'Question 1', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 1', NULL, '2017-08-23 03:04:19'),
(2, 'Question 2', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 4', NULL, '2017-08-23 03:02:01'),
(3, 'Question 3', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 3', NULL, '2017-08-23 03:04:20'),
(4, 'Question 4', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', NULL, NULL),
(5, 'Question 5', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 1', NULL, NULL),
(6, 'Question 6', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', NULL, NULL),
(7, 'Question 7', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 3', NULL, NULL),
(8, 'Question 8', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 1', NULL, NULL),
(9, 'Question 9', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', NULL, NULL),
(10, 'Question 10', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 3', NULL, NULL),
(11, 'Question 11', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 1', NULL, NULL),
(12, 'Question 12', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 4', NULL, NULL),
(13, 'Question 13', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', NULL, NULL),
(14, 'Question 14', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', NULL, '2017-08-23 03:04:20'),
(15, 'Question 15', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:46:55', '2017-08-24 01:46:55'),
(16, 'Question 16', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:46:55', '2017-08-24 01:46:55'),
(17, 'Question 17', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:46:55', '2017-08-24 01:46:55'),
(18, 'Question 18', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:46:55', '2017-08-24 01:46:55'),
(19, 'Question 19', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:46:55', '2017-08-24 01:46:55'),
(20, 'Question 20', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:46:55', '2017-08-24 01:46:55'),
(21, 'Question 21', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:46:55', '2017-08-24 01:46:55'),
(22, 'Question 22', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:46:55', '2017-08-24 01:46:55'),
(23, 'Question 15', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:48:26', '2017-08-24 01:48:26'),
(24, 'Question 16', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:48:26', '2017-08-24 01:48:26'),
(25, 'Question 17', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:48:26', '2017-08-24 01:48:26'),
(26, 'Question 18', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:48:26', '2017-08-24 01:48:26'),
(27, 'Question 19', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:48:26', '2017-08-24 01:48:26'),
(28, 'Question 20', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:48:26', '2017-08-24 01:48:26'),
(29, 'Question 21', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:48:26', '2017-08-24 01:48:26'),
(30, 'Question 22', 'Answer 1', 'Answer 2', 'Answer 3', 'Answer 4', 'Answer 2', '2017-08-24 01:48:26', '2017-08-24 01:48:26');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` bigint(20) DEFAULT '1' COMMENT '0: admin, 1: operator',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `mobile`, `remember_token`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$Z1DHEPZXbEHZrRrOC0EM/eIUKRKE96D5q8P23IWFOyDzefKo1Hp0.', NULL, 'EOVpW6K3AY6z0xoRwzADRjpnbgcOblPfpZ8pbGkbNaNgd7xICFEsPivS1CpH', 0, '2017-09-04 02:17:49', '2017-09-04 02:17:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `candidate_answers`
--
ALTER TABLE `candidate_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidate_answers_fk0` (`jobpost_id`),
  ADD KEY `candidate_answers_fk1` (`candidate_id`),
  ADD KEY `candidate_answers_fk2` (`question_id`);

--
-- Indexes for table `candidate_documents`
--
ALTER TABLE `candidate_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidate_documents_fk0` (`candidate_id`);

--
-- Indexes for table `candidate_experiences`
--
ALTER TABLE `candidate_experiences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidate_experiences_fk0` (`candidate_id`);

--
-- Indexes for table `candidate_jobs`
--
ALTER TABLE `candidate_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidate_jobs_fk0` (`candidate_id`),
  ADD KEY `candidate_jobs_fk1` (`jobpost_id`);

--
-- Indexes for table `candidate_qualifications`
--
ALTER TABLE `candidate_qualifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidate_qualifications_fk0` (`candidate_id`);

--
-- Indexes for table `candidate_results`
--
ALTER TABLE `candidate_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidate_results_fk0` (`jobpost_id`),
  ADD KEY `candidate_results_fk1` (`candidate_id`);

--
-- Indexes for table `cms_management`
--
ALTER TABLE `cms_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobposts`
--
ALTER TABLE `jobposts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `job_questions`
--
ALTER TABLE `job_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_questions_fk0` (`jobpost_id`),
  ADD KEY `job_questions_fk1` (`question_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`permission_id`,`user_id`,`user_type`);

--
-- Indexes for table `question_answers`
--
ALTER TABLE `question_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_details_fk0` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `candidate_answers`
--
ALTER TABLE `candidate_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `candidate_documents`
--
ALTER TABLE `candidate_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `candidate_experiences`
--
ALTER TABLE `candidate_experiences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `candidate_jobs`
--
ALTER TABLE `candidate_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `candidate_qualifications`
--
ALTER TABLE `candidate_qualifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `candidate_results`
--
ALTER TABLE `candidate_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `cms_management`
--
ALTER TABLE `cms_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobposts`
--
ALTER TABLE `jobposts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_questions`
--
ALTER TABLE `job_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `question_answers`
--
ALTER TABLE `question_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `candidate_answers`
--
ALTER TABLE `candidate_answers`
  ADD CONSTRAINT `candidate_answers_fk0` FOREIGN KEY (`jobpost_id`) REFERENCES `jobposts` (`id`),
  ADD CONSTRAINT `candidate_answers_fk1` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`),
  ADD CONSTRAINT `candidate_answers_fk2` FOREIGN KEY (`question_id`) REFERENCES `question_answers` (`id`);

--
-- Constraints for table `candidate_documents`
--
ALTER TABLE `candidate_documents`
  ADD CONSTRAINT `candidate_documents_fk0` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`);

--
-- Constraints for table `candidate_experiences`
--
ALTER TABLE `candidate_experiences`
  ADD CONSTRAINT `candidate_experiences_fk0` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`);

--
-- Constraints for table `candidate_jobs`
--
ALTER TABLE `candidate_jobs`
  ADD CONSTRAINT `candidate_jobs_fk0` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`),
  ADD CONSTRAINT `candidate_jobs_fk1` FOREIGN KEY (`jobpost_id`) REFERENCES `jobposts` (`id`);

--
-- Constraints for table `candidate_qualifications`
--
ALTER TABLE `candidate_qualifications`
  ADD CONSTRAINT `candidate_qualifications_fk0` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `candidate_results`
--
ALTER TABLE `candidate_results`
  ADD CONSTRAINT `candidate_results_fk0` FOREIGN KEY (`jobpost_id`) REFERENCES `jobposts` (`id`),
  ADD CONSTRAINT `candidate_results_fk1` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`);

--
-- Constraints for table `job_questions`
--
ALTER TABLE `job_questions`
  ADD CONSTRAINT `job_questions_fk0` FOREIGN KEY (`jobpost_id`) REFERENCES `jobposts` (`id`),
  ADD CONSTRAINT `job_questions_fk1` FOREIGN KEY (`question_id`) REFERENCES `question_answers` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_fk0` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
