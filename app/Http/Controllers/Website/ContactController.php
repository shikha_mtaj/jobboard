<?php

namespace App\Http\Controllers\Website;

use App\ContactEnquiry;
use App\Jobs\SendContactEnquiryLink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index()
    {
        return view('website.contact');
    }


    public function contactEmail(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'email' => 'required',
            'message' => 'required'
        ));

        $enquiry = ContactEnquiry::create([
            'name' => $request->name,
            'email' => $request->email,
            'message'=>$request->message
        ]);

        $this->dispatch(new SendContactEnquiryLink($enquiry));
        return redirect('')->with('message', 'Conatct enquiry has been sent successfully.');
    }

}
