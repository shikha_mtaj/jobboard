<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('appointments', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('code')->nullable();
            $table->integer('candidate_id')->index('appointments_fk2');
            $table->integer('city_id')->index('appointments_fk3');
            $table->integer('partner_id')->nullable();
            $table->text('resume', 65535)->nullable();
            $table->string('category_ids')->nullable();
            $table->string('start_time')->nullable();
            $table->string('end_time')->nullable();
            $table->string('date')->nullable();
            $table->string('total_experiences')->default('0');
            $table->string('joblocation_ids')->nullable();
            $table->string('country')->nullable();
            $table->integer('status')->comment('1:Approved, 0:Pending')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('appointments');
    }
}
