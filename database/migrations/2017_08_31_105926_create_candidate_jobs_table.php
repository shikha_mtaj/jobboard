<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('candidate_jobs', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('candidate_id')->index('candidate_jobs_fk0');
            $table->integer('jobpost_id')->index('candidate_jobs_fk1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('candidate_jobs');
    }
}
