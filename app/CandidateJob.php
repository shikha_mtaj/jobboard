<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateJob extends Model
{
    protected $fillable = ['candidate_id','jobpost_id','payment_type','challan_receipt','amount','payment_status','status'];

    public function jobDetail()
    {
        return $this->belongsTo(Jobpost::class,'jobpost_id','id');
    }

    public function candidateDetail()
    {
        return $this->belongsTo(Candidate::class,'candidate_id','id');
    }

}
