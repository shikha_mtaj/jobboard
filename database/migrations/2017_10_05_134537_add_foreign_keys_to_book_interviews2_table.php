<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToBookInterviews2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('book_interviews', function(Blueprint $table)
        {
            $table->foreign('category_id', 'book_interviews_fk5')->references('id')->on('categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('jobpost_id', 'book_interviews_fk6')->references('id')->on('jobposts')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('book_interviews', function(Blueprint $table)
        {
            $table->dropForeign('book_interviews_fk5');
            $table->dropForeign('book_interviews_fk6');
        });
    }
}
