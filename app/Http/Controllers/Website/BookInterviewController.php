<?php

namespace App\Http\Controllers\Website;

use App\BookInterview;
use App\Category;
use App\JobLocation;
use App\Jobpost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookInterviewController extends Controller
{
    // book interviews
    public function index(){

        $categories = Category::get();
        $joblocations = JobLocation::get();

        return view('website.bookinterview',['categories' => $categories,'joblocations' => $joblocations]);
    }

    // Store book interview
    public function storeBookInterview(Request $request){

        $this->validate($request, array(
            'category_id' =>'required',
            'resume' => 'mimes:doc,docx,pdf|max:1024',
            'date' => 'required',
            'start_time' => 'required',
            'joblocation_id' =>'required',
            'country' =>'required'
        ));

        $path = base_path('/assets/front/bookinterview');

        if ($request->file('resume') != "") {
            $photo = $request->file('resume');
            $input_resume = 'resume' . time() . '.' . $photo->getClientOriginalExtension();
            $destinationPath = $path;
            $photo->move($destinationPath, $input_resume);
        } else {
            $input_resume = "";
        }

        if(isset($request->jobpost_id) && !empty($request->jobpost_id)){
            $jobpost_id = $request->jobpost_id;
        } else {
            $jobpost_id = "";
        }

        $code = str_random(8).date('Ymd');

        $candidate = BookInterview::create([
            'code' => $code,
            'category_id' => $request->category_id,
            'jobpost_id' => $request->jobpost_id,
            'resume' => $input_resume,
            'date' => $request->date,
            'start_time' => $request->start_time,
            'location' => $request->joblocation_id,
            'country' => $request->country
        ]);

        return redirect(route('website.home'))->with('message', 'Book Interview details has been sent Successfully.');
    }

    // get jobs based on category using ajax
    public function getCategoryJobs(Request $request){

        $this->validate($request, ['categoryId' => 'required']);
        $categoryJobs = array();
        if($request['categoryId']){
            $categoryJobs = Jobpost::where(['category_id' => $request['categoryId'],'status' => 1 ])->get();
        }

        $result = "";
        if(isset($categoryJobs[0]) && !empty($categoryJobs[0])){

            $result .= '<label class="control-label">Jobs</label>';
            $result .= '<select name="jobpost_id" id="jobpost_id" class="form-control dropdown-product">';
            $result .= '<div class="search-category-container">';
            $result .= '<label class="styled-select">';
            foreach ($categoryJobs AS $job){
                $result .= '<option value="'.$job->id.'">'.$job->job_title.'</option>';
            }
            $result .= '</div></label></select>';
        }

        return response()->json(['result' => $result]);
    }

}
