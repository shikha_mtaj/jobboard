<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends User
{

    const Inactive = 0;
    const Active = 1;

    protected $fillable = ['name','email','password','resume','mobile','category_ids','gender','nationality'];


    /**
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function documents()
    {
        return $this->hasOne(CandidateDocument::class,'candidate_id','id');
    }

    public function jobPost()
    {
        return $this->belongsTo(Jobpost::class,'jobpost_id','id');
    }

    public function jobPosts()
    {
        return $this->hasMany(CandidateJob::class,'candidate_id','id');
    }


}
