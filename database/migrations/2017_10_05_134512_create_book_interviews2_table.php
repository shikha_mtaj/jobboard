<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookInterviews2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('book_interviews', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('code')->nullable();
            $table->integer('category_id')->index('book_interviews_fk5');
            $table->integer('jobpost_id')->index('book_interviews_fk6');
            $table->text('resume', 65535)->nullable();
            $table->string('date')->nullable();
            $table->string('start_time')->nullable();
            $table->string('location')->nullable();
            $table->string('country')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('book_interviews');
    }
}
