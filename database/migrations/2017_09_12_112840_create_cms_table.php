<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('title');
            $table->string('slug');
            $table->text('meta_keyword', 65535)->nullable();
            $table->text('meta_description', 65535)->nullable();
            $table->text('description', 65535)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cms');
    }
}
