@extends('mainlayout')

@section('title')
    Appointment Detail
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="{{ route('admin.candidate') }}">Candidate</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Appointment Details</a>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#">
                                    <i class="icon-bell"></i> Action</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-shield"></i> Another action</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-user"></i> Something else here</a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="#">
                                    <i class="icon-bag"></i> Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <div class="profile">
                <div class="tabbable-full-width">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <!--end row-->
                                    <div class="tabbable-custom-profile">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1_11">

                                                <div class="portlet-body">
                                                    <table class="table table-striped table-bordered table-advance table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th><i class="fa fa-briefcase"></i> Appointment Details </th>
                                                            <th class="hidden-xs"></th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><a href="javascript:;"> Appointment Code </a></td>
                                                            <td class="hidden-xs"> {{ (!empty($appointment->code)) ? $appointment->code : "-" }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Candidate Name </a></td>
                                                            <td class="hidden-xs"> {{ $appointment->candidate->name }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> City Name </a></td>
                                                            <td class="hidden-xs"> {{ $appointment->city->name }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Partner Name </a></td>
                                                            <td class="hidden-xs"> {{ (!empty($appointment->partner_id)) ? $appointment->partner->name : "-" }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Resume </a></td>
                                                            <td class="hidden-xs"> <a href="{{ asset('assets/front/appointment/'.$appointment->resume) }}" download="">Download</a> </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Categories </a></td>
                                                            <td class="hidden-xs"> {{ $appointment->categories }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Start Time </a></td>
                                                            <td class="hidden-xs"> {{ $appointment->start_time }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Date </a></td>
                                                            <td class="hidden-xs"> {{ $appointment->date }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Total Experiences </a></td>
                                                            <td class="hidden-xs"> {{ $appointment->total_experiences }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Job Locations </a></td>
                                                            <td class="hidden-xs"> {{ $appointment->joblocations }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Country </a></td>
                                                            <td class="hidden-xs"> {{ (!empty($appointment->country)) ? $appointment->country : "-" }} </td>
                                                            <td></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <br/>
                                                <br/>
                                                <a href="#" onClick="history.go(-1)"><< Back</a>
                                            </div>
                                            <!--tab-pane-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--tab_1_2-->
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('js')
@endsection