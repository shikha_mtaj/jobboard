@extends('mainlayout')

@section('title')
    Add Job Opening
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="{{ route('admin.job') }}">Job</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Add</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Add New Job Opening
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse">
                                        </a>
                                        <a href="#portlet-config" data-toggle="modal" class="config">
                                        </a>
                                        <a href="javascript:;" class="reload">
                                        </a>
                                        <a href="javascript:;" class="remove">
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="horizontal-form" method="post" action="{{ route('admin.storeJobOpening') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="jobpost_id" id="jobpost_id" value="{{ $id }}">
                                        <div class="form-body">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
                                                        <label class="control-label">Company Name</label>
                                                        <input type="text" name="company_name" id="company_name" class="form-control"  value="{{ old('company_name') }}">
                                                        @if ($errors->has('company_name'))
                                                            <span class="help-block">{{ $errors->first('company_name') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                                                        <label class="control-label">Start Date</label>
                                                        <input type="text" data-date-start-date="-{{ date('d') - 1 }}d" data-date="2012-02-12" data-date-format="yyyy-mm-dd" name="start_date" id="start_date" class="form-control date-picker" placeholder="yyyy-mm-dd" value="{{ old('start_date') }}" readonly>
                                                        @if ($errors->has('start_date'))
                                                            <span class="help-block">{{ $errors->first('start_date') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                                                        <label class="control-label">End Date</label>
                                                        <input type="text" data-date-start-date="-{{ date('d') - 1 }}d" data-date="2012-02-12" data-date-format="yyyy-mm-dd" name="end_date" id="end_date" class="form-control date-picker" placeholder="yyyy-mm-dd" value="{{ old('end_date') }}" readonly>
                                                        @if ($errors->has('end_date'))
                                                            <span class="help-block">{{ $errors->first('end_date') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('start_time') ? 'has-error' : '' }}">
                                                        <label class="control-label">Start Time</label>
                                                        <input type="text" name="start_time" id="start_time" class="form-control timepicker"  value="{{ old('start_time') }}">
                                                        @if ($errors->has('start_time'))
                                                            <span class="help-block">{{ $errors->first('start_time') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group {{ $errors->has('end_time') ? 'has-error' : '' }}">
                                                        <label class="control-label">End Time</label>
                                                        <input type="text" name="end_time" id="end_time" class="form-control timepicker"  value="{{ old('end_time') }}">
                                                        @if ($errors->has('end_time'))
                                                            <span class="help-block">{{ $errors->first('end_time') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <div class="form-group">
                                                        <label>Location</label>
                                                        <textarea class="ckeditor form-control" name="location" rows="6">{{ old('location') }}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--/span-->
                                        </div>
                                        <div class="form-actions right">
                                            <a href="{{ route('admin.viewJobOpenings',['id' => $id]) }}"><button type="button" class="btn default">Cancel</button></a>
                                            <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection

@section('js')

    <script type="text/javascript">
        $('.timepicker').timepicker();
    </script>

@endsection