<?php

namespace App\Http\Controllers\Website;

use App\Jobpost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobAlertController extends Controller
{
    //
    public function index(){

        $latestJobs = Jobpost::orderBy('id','desc')->get();
        return view('website.jobalerts' ,[ 'latestJobs' => $latestJobs ]);
    }
}
