<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    // Category dashboard
    public function index(){

        $categories = Category::get();
        return view('admin.category.index',[ 'categories' => $categories ]);
    }

    //add cms page
    public function addCategory(){
        return view('admin.category.addcategory');
    }

    public function storeCategory(Request $request){

        $this->validate($request ,array(
            'code' => 'required|unique:categories|max:255',
            'name' => 'required'
        ));

        $category = Category::create([
            'code' => $request->code,
            'name' => $request->name
        ]);

        $category->save();
        return redirect(route('admin.category'))->with('message','Category details have been saved successfully.');
    }

    public function editCategory($id){
        $category = Category::find($id);
        return view('admin.category.editcategory',['category'=>$category]);
    }

    public function updateCategory(Request $request ,$id){

        $this->validate($request ,array(
            'code' => 'required|unique:categories|max:255',
            'name' => 'required'
        ));

        $category = Category::find($id);

        $category->code = $request->code;
        $category->name = $request->name;
        $category->save();

        return redirect(route('admin.category'))->with('message', 'Category details has been updated Successfully.');

    }

    // delete category
    public function deleteCategory(Request $request)
    {
        $this->validate($request, [
            'categoryId' => 'required|exists:categories,id'
        ]);
        $category =Category::find($request->categoryId);
        $category->delete();
    }

    // Category Status Change
    public function changeStatus(Request $request)
    {
        $this->validate($request, [
            'categoryId' => 'required|exists:categories,id'
        ]);

        $category = Category::find($request['categoryId']);
        if ($request['status']) { $category->status = '0'; }
        else { $category->status = '1'; }

        $category->save();

        return response()->json([
            'status' => $request['status'] ? 0 : 1
        ]);
    }

}
