<?php

namespace App\Http\Controllers\Website;
use App\Appointment;
use App\Candidate;
use App\Category;
use App\ClientPartner;
use App\Http\Controllers\Controller;
use App\Jobpost;
use App\Testimonial;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $jobs=Jobpost::orderBy('id','DESC')->take(2)->where('status',1)->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->get();
        $categories=Category::limit(8)->where('status',1)->get();
        $partners = ClientPartner::get();
        $testimonials = Testimonial::get();

        $total_jobs = Jobpost::count();
        $total_candidates = Candidate::count();
        $total_appointments = Appointment::count();

        return view('website.index',['jobs'=>$jobs,'categories'=>$categories,'total_jobs' => $total_jobs ,'total_candidates'=>$total_candidates ,'total_appointments'=>$total_appointments ,'partners' => $partners ,'testimonials' => $testimonials ]);
    }
}
