@extends('mainlayout')

@section('title')
    Appointments
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Appointments</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>
            <img src="{{ asset('assets/loader.gif') }}" style="display: none;" class="loading-img" width="100px" height="100px">
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <div class="btn-group">
                                <a class="btn btn-primary send_mail" id="send_mail">
                                    Send Approval Mail
                                </a>
                            </div>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                            <tr>
                                <th></th>
                                <th>City</th>
                                <th>Categories</th>
                                <th>Resume</th>
                                <th>Date</th>
                                <th>Job Locations</th>
                                <th>Status</th>
                                <th width="45px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($appointments as $key=>$appointment)
                                <tr>
                                    <td style="text-align: center;">
                                        @if($appointment->status == 0)
                                            <input type="checkbox" name="appointments[]" value="{{ $appointment->id }}">
                                        @endif
                                    </td>
                                    <td>{{ $appointment->city->name }}</td>
                                    <td>{{ $appointment->categories }}</td>
                                    <td>
                                        @if( $appointment->resume!="")
                                        <a href="{{ asset('assets/front/appointment/'.$appointment->resume) }}" download="">Download</a>
                                        @endif
                                    </td>
                                    <td>{{ $appointment->date }}</td>
                                    <td>{{ $appointment->joblocations }}</td>
                                    <td style="text-align: center;">
                                        @if( $appointment->status == 1)
                                        <a href="javascript:void(0)" class="label label-sm status_check label-success">{{ "Approved" }}</a>
                                        @else
                                        <a href="javascript:void(0)" class="label label-sm status_check label-warning">{{ "Pending" }}</a>
                                        @endif
                                    </td>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-xs green dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a  href="{{ route('admin.viewAppointment',['id' => $appointment->id]) }}">
                                                        <i class="fa fa-pencil"></i> View Details </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" class="deleteAppointment" id="deleteAppointment" data-val="{{ $appointment->id }}">
                                                        <i class="fa fa-trash-o"></i> Delete </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->

@endsection

@section('js')

    <script type="text/javascript">

        $('.deleteAppointment').on('click', function(e){
            var appointmentId = $(this).attr('data-val');

            swal({
                title: 'Are you sure?',
                text: "You want to Delete this Appointment",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.deleteCandidateAppointment') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        appointmentId: appointmentId
                    },
                    success: function (data) {
                        swal({
                            title: 'Success!',
                            text: 'Appointment has been removed Successfully.',
                            type: "success",
                            confirmButtonText: "OK"
                        }).then(function () {
                            location.reload();
                        });
                    },
                    error: function (xhr, status, error) {
                        swal(
                            'Oops !',
                            'Something went wrong, please try again later.',
                            'warning'
                        );
                    }
                });
            });
        });

        $("#send_mail").click(function(){
            var appointments = [];
            $.each($("input[name='appointments[]']:checked"), function(){
                appointments.push($(this).val());
            });

            if(appointments != ""){
                swal({
                    title: 'Are you sure?',
                    text: "You want to approved this appointments",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('admin.approveCandidateAppointments') }}",
                        data: {
                            _token: '{{ csrf_token() }}',
                            appointments: appointments
                        },
                        beforeSend:function(){
                            $('.loading-img').css('display','');
                        },
                        success: function (data) {
                            swal({
                                title: 'Success!',
                                text: 'Appointment details have been sent successfully to candidate',
                                type: "success",
                                confirmButtonText: "OK"
                            }).then(function () {
                                location.reload();
                            });
                        },
                        error: function (xhr, status, error) {
                            $('.loading-img').css('display','none');
                            swal(
                                'Oops !',
                                'Something went wrong, please try again later.',
                                'warning'
                            );
                        }
                    });
                });
            } else {
                swal(
                    'Oops !',
                    'Please select appointments !!',
                    'warning'
                );
            }
        });

    </script>
@endsection