@extends('mainlayout')

@section('title')
    Candidate Detail
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#">
                                    <i class="icon-bell"></i> Action</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-shield"></i> Another action</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-user"></i> Something else here</a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="#">
                                    <i class="icon-bag"></i> Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> {{ ucfirst($candidate->name) }} </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="profile">
                <div class="tabbable-line tabbable-full-width">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" data-toggle="tab"> Overview </a>
                        </li>
                        <li>
                            <a href="#tab_1_3" data-toggle="tab"> More Information </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="list-unstyled profile-nav">
                                        <li>
                                            <img src="{{ asset('assets/front/candidate_documents/'.$candidate_documents->photo) }}" class="img-responsive pic-bordered" alt="" />
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-8 profile-info">
                                            <h1 class="font-green sbold uppercase">{{ ucfirst($candidate->name) }}</h1>
                                            <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt laoreet dolore magna aliquam tincidunt erat volutpat laoreet dolore magna aliquam tincidunt erat
                                                volutpat. </p>
                                        </div>
                                        <!--end col-md-8-->
                                    </div>
                                    <!--end row-->
                                    <div class="tabbable-line tabbable-custom-profile">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_11" data-toggle="tab"> Candidate Details </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1_11">

                                                <div class="portlet-body">
                                                    <table class="table table-striped table-bordered table-advance table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>
                                                                <i class="fa fa-briefcase"></i> Name </th>
                                                            <th class="hidden-xs">
                                                                <i class="fa fa-question"></i> Description </th>
                                                            <th> </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        <tr>
                                                            <td><a href="javascript:;"> Gender </a></td>
                                                            <td class="hidden-xs"> {{ $candidate->gender }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Date of Birth </a></td>
                                                            <td class="hidden-xs"> {{ $candidate->dob }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Phone No. </a></td>
                                                            <td class="hidden-xs"> {{ $candidate->phone }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Age </a></td>
                                                            <td class="hidden-xs"> {{ $candidate->age }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Present Address </a></td>
                                                            <td class="hidden-xs"> {{ $candidate->present_address }} </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:;"> Permanent Address </a></td>
                                                            <td class="hidden-xs"> {{ $candidate->permanent_address }} </td>
                                                            <td></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                            <!--tab-pane-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--tab_1_2-->
                        <div class="tab-pane" id="tab_1_3">
                            <div class="row profile-account">
                                <div class="col-md-3">
                                    <ul class="ver-inline-menu tabbable margin-bottom-10">
                                        <li class="active">
                                            <a data-toggle="tab" href="#tab_1-1">
                                                <i class="fa fa-cog"></i> Candidate Qualification </a>
                                            <span class="after"> </span>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#tab_2-2">
                                                <i class="fa fa-cog"></i> Candidate Experiences </a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#tab_3-3">
                                                <i class="fa fa-cog"></i> Candidate Documents </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <div class="tab-content">


                                        <div id="tab_1-1" class="tab-pane active">
                                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i> Candidate Qualification </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body flip-scroll">
                                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                                        <thead class="flip-content">
                                                        <tr>
                                                            <th width="20%"> Examination </th>
                                                            <th> Passing Year </th>
                                                            <th class="numeric"> Board / University </th>
                                                            <th class="numeric"> Percentage (%) </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($candidate_qualifications AS $qualification)
                                                        <tr>
                                                                <td> {{ $qualification->examination }} </td>
                                                                <td> {{ $qualification->year }} </td>
                                                                <td class="numeric"> {{ $qualification->board }} </td>
                                                                <td class="numeric"> {{ $qualification->percentage }} </td>
                                                        </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- END SAMPLE TABLE PORTLET-->
                                        </div>

                                        <div id="tab_2-2" class="tab-pane">
                                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i> Candidate Experiences </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body flip-scroll">
                                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                                        <thead class="flip-content">
                                                        <tr>
                                                            <th width="20%"> Post Name </th>
                                                            <th> Organization Name </th>
                                                            <th class="numeric"> Duration </th>
                                                            <th class="numeric"> Job Description </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($candidate_experiences AS $experience)
                                                            <tr>
                                                                <td> {{ $experience->post_name }} </td>
                                                                <td> {{ $experience->organization }} </td>
                                                                <td class="numeric"> {{ $experience->duration }} </td>
                                                                <td class="numeric"> {{ $experience->job_description }} </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- END SAMPLE TABLE PORTLET-->
                                        </div>

                                        <div id="tab_3-3" class="tab-pane">

                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>
                                                            <i class="fa fa-briefcase"></i> Name </th>
                                                        <th class="hidden-xs">
                                                            <i class="fa fa-question"></i> Description </th>
                                                        <th> </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <tr>
                                                        <td><a href="javascript:;"> Leaving Certificate </a></td>
                                                        <td class="hidden-xs"><a href="{{  asset('assets/front/candidate_documents/'.$candidate_documents->lc) }}" target="_blank"> View </a></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="javascript:;"> SSC Certificate </a></td>
                                                        <td class="hidden-xs"> <a href="{{  asset('assets/front/candidate_documents/'.$candidate_documents->ssc) }}" target="_blank">View</a></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="javascript:;"> Nursing Certificate </a></td>
                                                        <td class="hidden-xs"> <a href="{{  asset('assets/front/candidate_documents/'.$candidate_documents->nursery) }}" target="_blank">View</a></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="javascript:;"> Address Proof </a></td>
                                                        <td class="hidden-xs"> <a href="{{  asset('assets/front/candidate_documents/'.$candidate_documents->address_proof) }}" target="_blank">View</a></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="javascript:;"> Adhar Card </a></td>
                                                        <td class="hidden-xs"><a href="{{  asset('assets/front/candidate_documents/'.$candidate_documents->adhar_card) }}" target="_blank">View</a></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="javascript:;"> Signature </a></td>
                                                        <td class="hidden-xs"><a href="{{  asset('assets/front/candidate_documents/'.$candidate_documents->signature) }}" target="_blank">View</a></td>
                                                        <td></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <!--end col-md-9-->
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@endsection

@section('js')

@endsection