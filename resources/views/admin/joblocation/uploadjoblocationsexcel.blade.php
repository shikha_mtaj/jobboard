@extends('mainlayout')

@section('title')
    Upload Job Locations
@endsection

@section('css')
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
                Upload Job Locations
            </h3>
            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="{{ route('admin.uploadJobLocationsExcel') }}">Upload Job Locations</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form id="fileupload" class="horizontal-form" method="post" action="{{ route('admin.storeJobLocationsExcel') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="row fileupload-buttonbar">
                                        <div class="col-lg-7">
                                            <!-- The fileinput-button span is used to style the file input field as button -->

                                            <div class="form-group">
                                                <label for="exampleInputFile" class="col-md-3 control-label">File input</label>
                                                <div class="col-md-9">
                                                    <input type="file" name="file" id="exampleInputFile" class="btn green">
                                                </div>
                                            </div>
                                            @if ($errors->has('file'))
                                                <span class="help-block" style="color: red;">{{ $errors->first('file') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <br/><br/>
                                    Sample File : <a href="{{ asset('assets/sample.xls') }}" download> sample.xls </a>
                                    <div class="form-actions right">
                                        <button type="submit" class="btn blue start">
                                            <i class="fa fa-upload"></i>
                                            <span>Start upload </span>
                                        </button>
                                    </div>

                                    <!-- The table listing the files available for upload/download -->
                                    <table role="presentation" class="table table-striped clearfix">
                                        <tbody class="files">
                                        </tbody>
                                    </table>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection

@section('js')
@endsection
