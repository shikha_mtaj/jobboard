@extends('mainlayout')

@section('title')
    CMS
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->

            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="{{ route('admin.cms') }}">cms</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <div class="btn-group">
                                <a href="{{ route('admin.addCms')  }}" class="btn green">
                                    Add CMS <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Slug</th>
                                <th>Meta Keyword</th>
                                <th>Meta Description</th>
                                <th width="145px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cms_pages AS $cms)
                                <tr>
                                    <td>{{ $cms->title }}</td>
                                    <td>{{ $cms->slug }}</td>
                                    <td>{{ $cms->meta_keyword }}</td>
                                    <td>{{ $cms->meta_description }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-xs green dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a  href="{{ route('admin.editCms',['id' => $cms->id]) }}">
                                                        <i class="fa fa-pencil"></i> Edit </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection

@section('js')

@endsection