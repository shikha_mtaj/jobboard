<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->


<div class="panel-detail">
    <span class="badge badge-primary" style="font-size: 18px!important;"> Admin Panel</span>
</div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                <form class="sidebar-search " action="#" method="POST">
                  <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            <!-- BEGIN Dashboard LINK -->
            <li @if (Request::segment('2') === 'dashboard' || Request::segment('3') === 'dashboard') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.dashboard')  }}"><i class="icon-paper-plane"></i><span class="title"> Dashboard  </span>
                </a>
            </li>

            <li @if (Request::segment('2') === 'cms' || Request::segment('3') === 'cms') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.cms')  }}"><i class="icon-paper-plane"></i><span class="title"> CMS  </span>
                </a>
            </li>

            <li @if (Request::segment('2') === 'category' || Request::segment('3') === 'category') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.category')  }}"><i class="icon-paper-plane"></i><span class="title"> Category  </span>
                </a>
            </li>

            <li @if (Request::segment('2') === 'city' || Request::segment('3') === 'city') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.city')  }}"><i class="icon-paper-plane"></i><span class="title"> City  </span>
                </a>
            </li>

            <li @if (Request::segment('2') === 'joblocation' || Request::segment('3') === 'joblocation') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.jobLocation')  }}"><i class="icon-paper-plane"></i><span class="title"> Job Location </span>
                </a>
            </li>

            <li @if (Request::segment('2') === 'job' || Request::segment('3') === 'job') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.job')  }}"><i class="icon-paper-plane"></i><span class="title"> Job </span>
                </a>
            </li>

            <li @if (Request::segment('2') === 'candidate' || Request::segment('3') === 'candidate') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.candidate')  }}"><i class="icon-paper-plane"></i><span class="title"> Candidate </span>
                </a>
            </li>

            {{-- <li @if (Request::segment('2') === 'appointment' || Request::segment('3') === 'appointment') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.appointment')  }}"><i class="icon-paper-plane"></i>
                    <span class="title"> Appointment </span>
                </a>
            </li> --}}

            <li class="nav-item  @if (Request::segment('2') === 'appointment' || Request::segment('3') === 'appointment') active open @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-paper-plane"></i>
                    <span class="title">Appointment</span>
                    @if (Request::segment('2') === 'appointment' || Request::segment('3') === 'appointment')
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item @if (Request::segment('3') === 'pending') active open @endif">
                        <a href="{{ route('admin.appointment',['id' => 'pending']) }}" class="nav-link ">
                            <span class="title">Pending</span>
                        </a>
                    </li>
                    <li class="nav-item @if (Request::segment('3') === 'approved') active open @endif">
                        <a href="{{ route('admin.appointment',['id' => 'approved']) }}" class="nav-link ">
                            <span class="title">Approved</span>
                        </a>
                    </li>
                </ul>
            </li>


            <li @if (Request::segment('2') === 'enquiry' || Request::segment('3') === 'enquiry') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.enquiry')  }}"><i class="icon-paper-plane"></i><span class="title"> Enquiry </span>
                </a>
            </li>

            <li @if (Request::segment('2') === 'partner' || Request::segment('3') === 'partner') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.clientPartners')  }}"><i class="icon-paper-plane"></i><span class="title"> Clients & Partners </span>
                </a>
            </li>

            <li @if (Request::segment('2') === 'testimonial' || Request::segment('3') === 'testimonial') class="active" @endif data-container="body" data-placement="right" data-html="true">
                <a href="{{ route('admin.testimonial')  }}"><i class="icon-paper-plane"></i><span class="title"> Testimonials </span>
                </a>
            </li>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->