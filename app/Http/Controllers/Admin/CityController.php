<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\CityPartner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class CityController extends Controller
{
    // City Dashboard
    public function index(){

        $cities = City::get();
        return view('admin.city.index',[ 'cities' => $cities ]);
    }

    // Delete City
    public function deleteCity(Request $request)
    {
        $this->validate($request, [
            'deleteId' => 'required|exists:cities,id'
        ]);
        $city =City::find($request->deleteId);
        $city->delete();
    }

    // Delete All Cities
    public function deleteAllCities(Request $request){

        if($request['cities']!=""){
            $cities = City::wherein('id',$request['cities'])->get();
            foreach ($cities AS $city){
                $city =City::find($city->id);
                $city->delete();
            }
        }

    }

    public function uploadCitiesExcel(){
        return view('admin.city.uploadcitiesexcel');
    }

    // Upload New Cities
    public function storeCitiesExcel(Request $request){

        if($request->file('file') != ""){
            $image = $request->file('file');
            $excel_file = time().$image->getClientOriginalName();
            $destinationPath = base_path('/assets/excelfiles');
            $image->move($destinationPath, $excel_file);
        } else {
            return redirect(route('admin.uploadCitiesExcel'))->with('error','Please upload excel file.');
        }

        $address = "./assets/excelfiles/".$excel_file;

        Excel::load($address, function ($reader) {
            $results = $reader->get();
            foreach ($results->toArray() as $key => $value) {
                if(!empty($value['name'])) {
                    City::create([
                        'name' => $value['name']
                    ]);
                }
            }
        });

        return redirect(route('admin.city'))->with('message','Excel file data uploaded successfully.');


    }

    // view city partners
    public function viewCityPartners($id){
        $citypartners = CityPartner::where(['city_id'=>$id])->get();
        $city = City::find($id);
        return view('admin.city.viewcitypartners',['citypartners' => $citypartners ,'id' => $id ,'city' => $city]);
    }

    // add city partner
    public function addCityPartner($id){
        return view('admin.city.addcitypartner',['id' => $id]);
    }

    // store job opening
    public function storeCityPartner(Request $request){

        $this->validate($request ,array(
            'name' => 'required',
            'email' => 'required',
            'join_date' => 'required',
            'expiry_date' => 'required'
        ));

        CityPartner::create([
            'city_id' => $request->city_id,
            'name' => $request->name,
            'auth_person' => $request->auth_person,
            'address' => $request->address,
            'contact_no' => $request->contact_no,
            'email' => $request->email,
            'bonus' => $request->bonus,
            'join_date' => $request->join_date,
            'expiry_date' => $request->expiry_date,
            'description' => $request->description,
            'reference' => $request->reference,
            'approved_by' => 1,
            'status' => 1
        ]);

        return redirect(route('admin.viewCityPartners',['id' => $request->city_id]))->with('message','City Partner details has been saved successfully.');
    }

    // delete city partner
    public function deleteCityPartner(Request $request){

        $this->validate($request, [
            'cityPartnerId' => 'required|exists:city_partners,id'
        ]);

        $citypartner = CityPartner::find($request->cityPartnerId);
        $citypartner->delete();

    }

}
