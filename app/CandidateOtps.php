<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateOtps extends Model
{
   protected $fillable=['otp','mobile'];
}
