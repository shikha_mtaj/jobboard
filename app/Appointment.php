<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    // Appointment
    protected $fillable = ['code','candidate_id','city_id','partner_id','resume','category_ids','start_time','end_time','date','total_experiences','joblocation_ids','country'];

    public function city(){

        return $this->belongsTo(City::class,'city_id','id');
    }

    public function categories(){

        //return $this->belongsToMany(Category::class,'appointments','category_id','id');
        return $this->belongsToMany(Category::class,'categories','category_ids');
    }

    public function candidate(){

        return $this->belongsTo(Candidate::class,'candidate_id','id');
    }

    public function partner(){

        return $this->belongsTo(CityPartner::class,'partner_id','id');
    }


}
