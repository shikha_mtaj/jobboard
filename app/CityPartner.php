<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityPartner extends Model
{
    //
    protected $fillable = ['city_id','code','name','auth_person','address','contact_no','email','bonus','join_date','expiry_date','description','reference','approved_by','status'];

    public function cityDetail(){
        return $this->belongsTo(City::class,'city_id','id');

    }
}
