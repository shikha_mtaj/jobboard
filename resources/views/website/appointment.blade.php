@extends('website.pageLayout')

@section('css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')

    <!-- Content section Start -->
    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-9 col-md-offset-2">
                    <div class="page-ads box">
                        <form class="form-ad" method="POST" action="{{ route('website.storeAppointment') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="divider"><h3>Appointment</h3></div>
                            <div class="form-group {{$errors->has('city_id')? 'has-error':''}}">
                                <label class="control-label">City</label>
                                        <select name="city_id" id="city_id" class="dropdown-product form-control">
                                            <option value="">Select City</option>
                                            @foreach($cities AS $city)
                                            <option value="{{ $city->id }}" @if(old('city_id')==$city->id) selected @endif>{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                            </div>

                            <div class="form-group partner_id {{$errors->has('partner_id')? 'has-error':''}}">
                            </div>

                            <div class="form-group {{$errors->has('category_ids[]')? 'has-error':''}}">
                                <label class="control-label">Category</label>
                                <div class="search-category-container">
                                    <label class="styled-select">
                                        <select name="category_ids[]" id="category_ids" class="dropdown-product selectpicker form-control" multiple>
                                            @foreach($categories AS $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group {{$errors->has('resume')? 'has-error':''}}">
                                <div class="button-group">
                                    <div class="action-buttons">
                                        <div class="upload-button">
                                            <button class="btn btn-common btn-sm">Upload Resume</button>
                                            <input name="resume" id="cover_img_file" type="file">
                                            <label>(Max Size : 1 MB )</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group {{$errors->has('date')? 'has-error':''}}">
                                <label class="control-label">Date</label>
                                <input type="text" name="date" id="date" class="form-control" placeholder="yyyy-mm-dd"  value="{{ old('date') }}">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Start Time</label>
                                <input type="text" name="start_time" id="start_time" class="form-control timepicker" value="{{ old('start_time') }}">
                            </div>

                            <div class="form-group {{$errors->has('total_experiences')? 'has-error':''}}">
                                <label class="control-label">Total Experiences</label>
                                        <select name="total_experiences" id="total_experiences" class="dropdown-product form-control">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                            </div>

                            <div class="form-group {{$errors->has('joblocation_ids[]')? 'has-error':''}}">
                                <label class="control-label">Job Location</label>
                                        <select name="joblocation_ids[]" id="joblocation_ids" class="dropdown-product form-control">
                                            @foreach($joblocations AS $location)
                                                <option value="{{ $location->id }}">{{ $location->name }}</option>
                                            @endforeach
                                        </select>
                            </div>
                            <div class="form-group {{$errors->has('country')? 'has-error':''}}">
                                <label class="control-label">Country</label>
                                        <select name="country" id="country" class="dropdown-product form-control">
                                            <option value="India">India</option>
                                            <option value="Europe">Europe</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Australia">Australia</option>
                                            <option value="New Zealand">New Zealand</option>
                                            <option value="UAE">UAE</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Singapore">Singapore</option>
                                        </select>
                            </div>
                            <input type="submit" class="btn btn-common" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Content section End -->
@endsection

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">

        $('.timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 15,
            minTime: '10',
            maxTime: '6:00pm',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

        $( "#date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        $('#city_id').change(function () {
            var cityId = $(this).val();

            $.ajax({
                type: "POST",
                url: "{{ route('website.getCityPartners') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    cityId: cityId
                },
                success: function (data) {
                    $('.partner_id').html(data.result);
                },
                error: function (xhr, status, error) {
                    swal(
                        'Oops !',
                        'Something went wrong, please try again later.',
                        'warning'
                    );
                }
            });
        });

    </script>

@endsection