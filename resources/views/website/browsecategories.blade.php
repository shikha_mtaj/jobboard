@extends('website.pageLayout')

@section('content')

    <!-- Page Header Start -->
    <div class="page-header" style="background: url({{ URL::asset('assets/img/banner1.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Categories</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('website.home') }}"><i class="ti-home"></i> Home</a></li>
                            <li class="current">Browse Categories</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <!-- Category Section Start -->
    <section class="section" >
        <div class="container">
            <h2 class="section-title">Browse Categories</h2>
            <div class="row">
                <div class="col-md-12">
                    @foreach($categories AS $category)
                    <div class="col-md-3 col-sm-3 col-xs-12 f-category">
                        <a href="{{ route('website.browseCategoryJobs',['id' => $category->id]) }}">
                            {{--<div class="icon">
                                <i class="ti-home"></i>
                            </div>--}}
                            <h3>{{ $category->name }}</h3>
                            <p>{{ $category->categoryJobs()->count() }} jobs</p>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- Category Section End -->

@endsection