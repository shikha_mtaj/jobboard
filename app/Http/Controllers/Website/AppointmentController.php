<?php

namespace App\Http\Controllers\Website;

use App\Appointment;
use App\Category;
use App\City;
use App\CityPartner;
use App\JobLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{
    //
    public function index(){

        $categories = Category::get();
        $cities = City::get();
        $joblocations = JobLocation::get();

        return view('website.appointment',['categories' => $categories ,'cities' => $cities ,'joblocations' => $joblocations]);

    }

    public function storeAppointment(Request $request){

        $this->validate($request, array(
            'city_id' => 'required',
            'resume' => 'mimes:doc,docx,pdf|max:1024',
            'date' => 'required',
            'total_experiences' =>'required',
            'category_ids' =>'required',
            'joblocation_ids' =>'required'
        ));

        $path = base_path('/assets/front/appointment');

        if ($request->file('resume') != "") {
            $photo = $request->file('resume');
            $input_resume = 'resume' . time() . '.' . $photo->getClientOriginalExtension();
            $destinationPath = $path;
            $photo->move($destinationPath, $input_resume);
        } else {
            $input_resume = "";
        }

        if(!empty($request->category_ids)){
            $category_ids = implode(',',$request->category_ids);
        } else {
            $category_ids = "";
        }

        if(!empty($request->joblocation_ids)){
            $joblocation_ids = implode(',',$request->joblocation_ids);
        } else {
            $joblocation_ids = "";
        }
        if(isset($request->partner_id) && !empty($request->partner_id)){
            $parther_id = $request->partner_id;
        } else {
            $parther_id = "";
        }

        $code = str_random(8).date('Ymd');

        $candidate = Appointment::create([
            'code' => $code,
            'candidate_id' => Auth::guard('candidate')->user()->id,
            'city_id' => $request->city_id,
            'partner_id' => $parther_id,
            'resume' => $input_resume,
            'category_ids' => $category_ids,
            'start_time' => $request->start_time,
            'date' => $request->date,
            'total_experiences' => $request->total_experiences,
            'joblocation_ids' => $joblocation_ids,
            'country' => $request->country,
            'status' => 0
        ]);

        return redirect(route('website.home'))->with('message', 'Appointment details has been sent Successfully.');

    }

    // Partners get based on selected city using ajax
    public function getCityPartners(Request $request){

        $this->validate($request, ['cityId' => 'required']);
        $cityPartners = array();
        if($request['cityId']){
            $cityPartners = CityPartner::where(['city_id' => $request['cityId']])->get();
        }

        $result = "";
        if(isset($cityPartners[0]) && !empty($cityPartners[0])){

            $result .= '<label class="control-label">Partners</label>';
            $result .= '<select name="partner_id" id="partner_id" class="form-control dropdown-product">';
            $result .= '<div class="search-category-container">';
            $result .= '<label class="styled-select">';
            foreach ($cityPartners AS $partner){
                $result .= '<option value="'.$partner->id.'">'.$partner->name.'</option>';
            }
            $result .= '</div></label></select>';

        }


        return response()->json(['result' => $result]);
    }

}
