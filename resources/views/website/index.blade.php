@extends('website.homeLayout')

@section('content')

<div class="search-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Find the job that fits your life</h1><br><h2>More than <strong>12,000</strong> jobs are waiting to Kickstart your career!</h2>

                <div class="content">
                    <form method="POST" action="{{  route('website.searchResult') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" name="title" id="title" type="text" placeholder="job title / keywords ">
                                    <i class="ti-time"></i>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="search-category-container">
                                    <label class="styled-select">
                                        <select name="category_id" id="category_id" class="dropdown-product selectpicker">
                                            <option value="">All Categories</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-6">
                                <button type="submit" class="btn btn-search-icon"><i class="ti-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="popular-jobs">
                    <b>Popular Keywords: </b>
                    <a href="#">Web Design</a>
                    <a href="#">Manager</a>
                    <a href="#">Programming</a>
                </div>


            </div>
        </div>
    </div>
</div>
</section>
<!-- end intro section -->
</div>

<!-- Find Job Section Start -->
<section class="find-job section">
    <div class="container">
        <h2 class="section-title">Hot Jobs</h2>
        <div class="row">
            <div class="col-md-12">

                @foreach($jobs as $job)

                <div class="job-list">
                    <div class="thumb">
                        <a href="{{ route('website.job_details',['id'=>$job->id]) }}"><img style="width:100px" src="{{ asset('assets/images/job/'.$job->image)}}" alt=""></a>
                    </div>
                    <div class="job-list-content">
                        <h4><a href="{{ route('website.job_details',['id'=>$job->id])}}">{{$job->job_title}}</a><span class="full-time">{{$job->job_type}}</span></h4>
                        <p>{!! $job->short_description !!}</p>
                        <div class="job-tag">
                            <div class="pull-left">
                                <div class="meta-tag">
                                    <span><a href="#"><i class="ti-desktop"></i>{{ $job->category->name }}</a></span>
                                    {{--<span><i class="ti-location-pin"></i>Cupertino, CA, USA</span>--}}
                                    {{--<span><i class="ti-time"></i>60/Hour</span>--}}
                                </div>
                            </div>
                            <div class="pull-right">

                                <a href="{{ route('website.job_details',['id'=>$job->id]) }}" class="btn btn-common btn-rm">More Detail</a>
                            </div>
                        </div>
                    </div>
                </div>

                @endforeach

            </div>
            <a href="{{ route('website.browseJobs') }}" style="float: right;"><b>View More >></b></a>
            {{--<div class="col-md-12">--}}
                {{--<div class="showing pull-left">--}}
                    {{--<a href="#">Showing <span>6-10</span> Of 24 Jobs</a>--}}
                {{--</div>--}}
                {{--<ul class="pagination pull-right">--}}
                    {{--<li class="active"><a href="#" class="btn btn-common" ><i class="ti-angle-left"></i> prev</a></li>--}}
                    {{--<li><a href="#">1</a></li>--}}
                    {{--<li><a href="#">2</a></li>--}}
                    {{--<li><a href="#">3</a></li>--}}
                    {{--<li><a href="#">4</a></li>--}}
                    {{--<li><a href="#">5</a></li>--}}
                    {{--<li class="active"><a href="#" class="btn btn-common">Next <i class="ti-angle-right"></i></a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>
</section>
<!-- Find Job Section End -->

<!-- Category Section Start -->
<section class="category section" >
    <div class="container">
        <h2 class="section-title">Browse Categories</h2>
        <div class="row">
            <div class="col-md-12">
                @foreach($categories as $category)
                <div class="col-md-3 col-sm-3 col-xs-12 f-category">
                    <a href="{{ route('website.browseCategoryJobs',['id' => $category->id]) }}">
                        {{--  <div class="icon">
                            <i class="ti-desktop"></i>
                        </div>--}}
                        <h3>{{ $category->name}}</h3>
                        <p>{{ \App\Jobpost::where('category_id', $category->id)->count() }} jobs</p>
                    </a>
                </div>
                    @endforeach

                <a style="float: right;" href="{{ route('website.browseCategories') }}"><b>View More >>></b></a>


            </div>
        </div>
    </div>
</section>
<!-- Category Section End -->

<!-- Featured Jobs Section Start -->
{{--
<section class="featured-jobs section">
    <div class="container">
        <h2 class="section-title">
            Featured Jobs
        </h2>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="featured-item">
                    <div class="featured-wrap">
                        <div class="featured-inner">
                            <figure class="item-thumb">
                                <a class="hover-effect" href="job-page.html">
                                    <img src="assets/img/features/img-1.jpg" alt="">
                                </a>
                            </figure>
                            <div class="item-body">
                                <h3 class="job-title"><a href="job-page.html">Graphic Designer</a></h3>
                                <div class="adderess"><i class="ti-location-pin"></i> Dallas, United States</div>
                            </div>
                        </div>
                    </div>
                    <div class="item-foot">
                        <span><i class="ti-calendar"></i> 4 months ago</span>
                        <span><i class="ti-time"></i> Full Time</span>
                        <div class="view-iocn">
                            <a href="job-page.html"><i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="featured-item">
                    <div class="featured-wrap">
                        <div class="featured-inner">
                            <figure class="item-thumb">
                                <a class="hover-effect" href="job-page.html">
                                    <img src="assets/img/features/img-2.jpg" alt="">
                                </a>
                            </figure>
                            <div class="item-body">
                                <h3 class="job-title"><a href="job-page.html">Software Engineer</a></h3>
                                <div class="adderess"><i class="ti-location-pin"></i> Delaware, United States</div>
                            </div>
                        </div>
                    </div>
                    <div class="item-foot">
                        <span><i class="ti-calendar"></i> 5 months ago</span>
                        <span><i class="ti-time"></i> Part Time</span>
                        <div class="view-iocn">
                            <a href="job-page.html"><i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="featured-item">
                    <div class="featured-wrap">
                        <div class="featured-inner">
                            <figure class="item-thumb">
                                <a class="hover-effect" href="job-page.html">
                                    <img src="assets/img/features/img-3.jpg" alt="">
                                </a>
                            </figure>
                            <div class="item-body">
                                <h3 class="job-title"><a href="job-page.html">Managing Director</a></h3>
                                <div class="adderess"><i class="ti-location-pin"></i> NY, United States</div>
                            </div>
                        </div>
                    </div>
                    <div class="item-foot">
                        <span><i class="ti-calendar"></i> 3 months ago</span>
                        <span><i class="ti-time"></i> Full Time</span>
                        <div class="view-iocn">
                            <a href="job-page.html"><i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="featured-item">
                    <div class="featured-wrap">
                        <div class="featured-inner">
                            <figure class="item-thumb">
                                <a class="hover-effect" href="job-page.html">
                                    <img src="assets/img/features/img-3.jpg" alt="">
                                </a>
                            </figure>
                            <div class="item-body">
                                <h3 class="job-title"><a href="job-page.html">Graphic Designer</a></h3>
                                <div class="adderess"><i class="ti-location-pin"></i> Washington, United States</div>
                            </div>
                        </div>
                    </div>
                    <div class="item-foot">
                        <span><i class="ti-calendar"></i> 1 months ago</span>
                        <span><i class="ti-time"></i> Part Time</span>
                        <div class="view-iocn">
                            <a href="job-page.html"><i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="featured-item">
                    <div class="featured-wrap">
                        <div class="featured-inner">
                            <figure class="item-thumb">
                                <a class="hover-effect" href="job-page.html">
                                    <img src="assets/img/features/img-2.jpg" alt="">
                                </a>
                            </figure>
                            <div class="item-body">
                                <h3 class="job-title"><a href="job-page.html">Software Engineer</a></h3>
                                <div class="adderess"><i class="ti-location-pin"></i> Dallas, United States</div>
                            </div>
                        </div>
                    </div>
                    <div class="item-foot">
                        <span><i class="ti-calendar"></i> 6 months ago</span>
                        <span><i class="ti-time"></i> Full Time</span>
                        <div class="view-iocn">
                            <a href="job-page.html"><i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="featured-item">
                    <div class="featured-wrap">
                        <div class="featured-inner">
                            <figure class="item-thumb">
                                <a class="hover-effect" href="job-page.html">
                                    <img src="assets/img/features/img-1.jpg" alt="">
                                </a>
                            </figure>
                            <div class="item-body">
                                <h3 class="job-title"><a href="job-page.html">Managing Director</a></h3>
                                <div class="adderess"><i class="ti-location-pin"></i> NY, United States</div>
                            </div>
                        </div>
                    </div>
                    <div class="item-foot">
                        <span><i class="ti-calendar"></i> 7 months ago</span>
                        <span><i class="ti-time"></i> Part Time</span>
                        <div class="view-iocn">
                            <a href="job-page.html"><i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- Featured Jobs Section End -->

<!-- Start Purchase Section -->
<section class="section purchase" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="section-content text-center">
                <h1 class="title-text">
                    Looking for a Job
                </h1>
                <p>Join thousand of employers and earn what you deserve!</p>
                <a href="{{ route('website.browseJobs') }}" class="btn btn-common">Get Started Now</a>
            </div>
        </div>
    </div>
</section>
<!-- End Purchase Section -->

<!-- Blog Section -->
{{--
<section id="blog" class="section">
    <!-- Container Starts -->
    <div class="container">
        <h2 class="section-title">
            Latest News
        </h2>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 blog-item">
                <!-- Blog Item Starts -->
                <div class="blog-item-wrapper">
                    <div class="blog-item-img">
                        <a href="single-post.html">
                            <img src="assets/img/blog/home-items/img1.jpg" alt="">
                        </a>
                    </div>
                    <div class="blog-item-text">
                        <div class="meta-tags">
                            <span class="date"><i class="ti-calendar"></i> Dec 20, 2017</span>
                            <span class="comments"><a href="#"><i class="ti-comment-alt"></i> 5 Comments</a></span>
                        </div>
                        <a href="single-post.html">
                            <h3>
                                Tips to write an impressive resume online for beginner
                            </h3>
                        </a>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore praesentium asperiores ad vitae.
                        </p>
                        <a href="single-post.html" class="btn btn-common btn-rm">Read More</a>
                    </div>
                </div>
                <!-- Blog Item Wrapper Ends-->
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 blog-item">
                <!-- Blog Item Starts -->
                <div class="blog-item-wrapper">
                    <div class="blog-item-img">
                        <a href="single-post.html">
                            <img src="assets/img/blog/home-items/img2.jpg" alt="">
                        </a>
                    </div>
                    <div class="blog-item-text">
                        <div class="meta-tags">
                            <span class="date"><i class="ti-calendar"></i> Jan 20, 2018</span>
                            <span class="comments"><a href="#"><i class="ti-comment-alt"></i> 5 Comments</a></span>
                        </div>
                        <a href="single-post.html">
                            <h3>
                                Let's explore 5 cool new features in JobBoard theme
                            </h3>
                        </a>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore praesentium asperiores ad vitae.
                        </p>
                        <a href="single-post.html" class="btn btn-common btn-rm">Read More</a>
                    </div>
                </div>
                <!-- Blog Item Wrapper Ends-->
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 blog-item">
                <!-- Blog Item Starts -->
                <div class="blog-item-wrapper">
                    <div class="blog-item-img">
                        <a href="single-post.html">
                            <img src="assets/img/blog/home-items/img3.jpg" alt="">
                        </a>
                    </div>
                    <div class="blog-item-text">
                        <div class="meta-tags">
                            <span class="date"><i class="ti-calendar"></i> Mar 20, 2018</span>
                            <span class="comments"><a href="#"><i class="ti-comment-alt"></i> 5 Comments</a></span>
                        </div>
                        <a href="single-post.html">
                            <h3>
                                How to convince recruiters and get your dream job
                            </h3>
                        </a>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore praesentium asperiores ad vitae.
                        </p>
                        <a href="single-post.html" class="btn btn-common btn-rm">Read More</a>
                    </div>
                </div>
                <!-- Blog Item Wrapper Ends-->
            </div>
        </div>
    </div>
</section>
--}}
<!-- blog Section End -->

<!-- Clients Section -->
<section class="clients section">
    <div class="container">
        <h2 class="section-title">
            Clients & Partners
        </h2>
        <div class="row">
            <div id="clients-scroller">
                @if(!empty($partners))
                    @foreach($partners AS $partner)
                    <div class="items">
                        <a href="{{ (!empty($partner->link)) ? $partner->link : "" }}" target="_blank"><img src="{{ asset('assets/front/clientspartners/'.$partner->image) }}" alt=""></a>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
<!-- Client Section End -->

<!-- Testimonial Section Start -->
<section id="testimonial" class="section">
    <div class="container">
        <div class="row">
            <div class="touch-slider" class="owl-carousel owl-theme">
                @if(!empty($testimonials[0]))
                    @foreach($testimonials AS $key => $testimonial)
                    <div class="item active text-center">
                        @if(!empty($testimonial->image))
                            <img class="img-member" src="{{ asset('assets/front/testimonials/'.$testimonial->image) }}" alt="">
                        @else
                            <img class="img-member" src="{{ asset('assets/img/no-user.jpg') }}" alt="">
                        @endif

                        <div class="client-info">
                            <h2 class="client-name">{{ $testimonial->name }} <span>({{ $testimonial->designation }})</span></h2>
                        </div>
                        <p>{!! $testimonial->description !!}</p>
                    </div>
                     @endforeach
                 @endif
            </div>
        </div>
    </div>
</section>
<!-- Testimonial Section End -->

<!-- Counter Section Start -->
<section id="counter">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="counting">
                    <div class="icon">
                        <i class="ti-briefcase"></i>
                    </div>
                    <div class="desc">
                        <h2>Jobs</h2>
                        <h1 class="counter">{{ $total_jobs + 50 }}</h1>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="counting">
                    <div class="icon">
                        <i class="ti-user"></i>
                    </div>
                    <div class="desc">
                        <h2>Members</h2>
                        <h1 class="counter">{{ $total_candidates + 100 }}</h1>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="counting">
                    <div class="icon">
                        <i class="ti-write"></i>
                    </div>
                    <div class="desc">
                        <h2>Appointments</h2>
                        <h1 class="counter">{{ $total_appointments + 150 }}</h1>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="counting">
                    <div class="icon">
                        <i class="ti-heart"></i>
                    </div>
                    <div class="desc">
                        <h2>Resumes</h2>
                        <h1 class="counter">{{ $total_candidates + 100 }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Counter Section End -->

@endsection