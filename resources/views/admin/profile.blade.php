@extends('mainlayout')

@section('title')
    Profile
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
                Profile
            </h3>
            <br/>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Profile</a>

                    </li>

                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <br/>
            <!-- BEGIN PAGE CONTENT-->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Profile Details</span>
                                </div>

                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!-- PERSONAL INFO TAB -->

                                        <form role="form" method="post" action="{{ route('admin.updateProfile') }}">

                                            {{ csrf_field() }}
                                            <div class="form-group {{ $errors->has('name')? 'has-error':'' }}">
                                                <label class="control-label"> Name</label>
                                                <input type="text" placeholder="Name" name="name" value="{{ Auth::user()->name }}" class="form-control">
                                                @if ($errors->has('name'))
                                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group  {{ $errors->has('email')? 'has-error':'' }}">
                                                <label class="control-label">Email</label>
                                                <input type="text" placeholder="Email" name="email" value="{{ Auth::user()->email }}" class="form-control">
                                                @if ($errors->has('email'))
                                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group  {{ $errors->has('mobile')? 'has-error':'' }}">
                                                <label class="control-label">Mobile No.</label>
                                                <input type="text" placeholder="Mobile No." name="mobile" value="{{ Auth::user()->mobile }}" class="form-control">
                                                @if ($errors->has('mobile'))
                                                    <span class="help-block">{{ $errors->first('mobile') }}</span>
                                                @endif
                                            </div>


                                            <div class="margiv-top-10">
                                               <input type="submit"  class="btn green-haze">

                                            </div>
                                        </form>

                                    <!-- END PERSONAL INFO TAB -->
                                    <!-- CHANGE AVATAR TAB -->

                                    </div>
                                    <!-- END PRIVACY SETTINGS TAB -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection
