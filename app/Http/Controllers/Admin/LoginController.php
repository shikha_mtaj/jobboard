<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public  function loginForm(){

        return view('admin.login');

    }

    public function authenticate(Request $request)
    {

        $this->validate($request,[
            'email'=>'required',
            'password'=>'required'
        ]);

        if(Auth::attempt([
            'email'=>$request->email,
            'password'=>$request->password,

        ]))
        {
            return redirect('admin/dashboard');
        }
        else{
            return redirect()->back()->with('error','Invalid Username or Password');
        }
    }

    public function logout()
    {
       Auth::logout();
        return redirect('admin/dashboard');

    }

}
