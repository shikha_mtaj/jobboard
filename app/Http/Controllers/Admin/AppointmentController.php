<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Category;
use App\JobLocation;
use App\Jobs\SendAppointmentLink;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppointmentController extends Controller
{

    // view appointments
    public function index($id){
        if($id == "pending"){ $status = 0; } else { $status = 1; }
        $appointments = Appointment::where('status',$status)->get();

        foreach ($appointments AS $key => $appointment){
            $categories = Category::whereIn('id',explode(',',$appointment->category_ids))->pluck('name')->toArray();
            $joblocations = JobLocation::whereIn('id',explode(',',$appointment->joblocation_ids))->pluck('name')->toArray();
            $appointments[$key]['categories'] = implode(',',$categories);
            $appointments[$key]['joblocations'] = implode(',',$joblocations);
        }

        return view('admin.appointment.appointments',[ 'appointments' => $appointments ,'appointments_status' => $id ]);
    }

    // delete appointment
    public function deleteAppointment(Request $request)
    {
        $this->validate($request, [
            'appointmentId' => 'required|exists:appointments,id'
        ]);
        $appointment =Appointment::find($request->appointmentId);
        $appointment->delete();
    }

    // Approve candidate appointments
    public function approveAppointments(Request $request)
    {
        if($request['appointments'] != ""){
            $appointments = Appointment::wherein('id',$request['appointments'])->get();

            foreach ($appointments AS $key => $appointment){
                $categories = Category::whereIn('id',explode(',',$appointment->category_ids))->pluck('name')->toArray();
                $joblocations = JobLocation::whereIn('id',explode(',',$appointment->joblocation_ids))->pluck('name')->toArray();
                $appointments[$key]['categories'] = implode(',',$categories);
                $appointments[$key]['joblocations'] = implode(',',$joblocations);
            }

            foreach ($appointments AS $key => $appointment){
                $data = array('appointment' => $appointment);
                $pdfName = str_replace(' ','',$appointment->candidate->name) . time();
                $pdfPath = base_path('assets/front/appointment/' . $pdfName . '.pdf');
                PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
                PDF::loadView('invoice', $data)->save($pdfPath);

                $this->dispatch(new SendAppointmentLink($appointment ,$pdfPath));

                Appointment::where(['id' => $appointment->id])->update(['status' => 1]);

            }
        }
    }

    public function viewAppointment($id){

        $appointment = Appointment::where('id',$id)->first();

        $categories = Category::whereIn('id',explode(',',$appointment->category_ids))->pluck('name')->toArray();
        $joblocations = JobLocation::whereIn('id',explode(',',$appointment->joblocation_ids))->pluck('name')->toArray();
        $appointment['categories'] = implode(',',$categories);
        $appointment['joblocations'] = implode(',',$joblocations);

        return view('admin.appointment.viewappointment',['appointment' => $appointment]);

    }




}
